package com.pack.commonmethods;

import java.awt.AWTException;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class commonMethods 
{
private WebDriver driver;  
	
	public commonMethods(WebDriver driver){
		this.driver = driver;
	}
	
	public void navigateBack(){
		driver.navigate().back();
	}
	
	public void waitTenSec() throws InterruptedException{
		Thread.sleep(10000);
	}
	
	public void waitNineSec() throws InterruptedException{
		Thread.sleep(9000);
	}
	
	public void waitEightSec() throws InterruptedException{
		Thread.sleep(8000);
	}
	
	public void waitSevenSec() throws InterruptedException{
		Thread.sleep(7000);
	}
	public void waitSixSec() throws InterruptedException{
		Thread.sleep(6000);
	}
	public void waitFiveSec() throws InterruptedException{
		Thread.sleep(5000);
	}
	public void waitFourSec() throws InterruptedException{
		Thread.sleep(4000);
	}
	public void waitThreeSec() throws InterruptedException{
		Thread.sleep(3000);
	}
	public void waitTwoSec() throws InterruptedException{
		Thread.sleep(2000);
	}
	public void waitOneSec() throws InterruptedException{
		Thread.sleep(1000);
	}
	public void hardRefresh() throws AWTException{
		Robot hardRef = new Robot();
		hardRef.keyPress(KeyEvent.VK_CONTROL);
		hardRef.keyRelease(KeyEvent.VK_CONTROL);
		
		hardRef.keyPress(KeyEvent.VK_F5);
		hardRef.keyRelease(KeyEvent.VK_F5);
	}
	
	public void pressDownButton() throws AWTException{
		Robot downButtonOnce = new Robot();
		downButtonOnce.keyPress(KeyEvent.VK_DOWN);
		downButtonOnce.keyRelease(KeyEvent.VK_DOWN);
		
	}
	
	public void scrolDown_150Pixcels()
	{
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,150)");
	}
	
	public void scrolup_150Pixcels()
	{
		((JavascriptExecutor) driver).executeScript("window.scrollBy(0,-150)");
	}
	
	public void openNewTab()

	{
		System.out.println("\t< New Tab is opened >");
	    driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
	    ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs.get(0));

	}
	public void switchTo_previousTab()
	{
		System.out.println("\t < Navigated to Previous Tab >");
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.PAGE_UP);
		String tab_handle = driver.getWindowHandle();
	    driver.switchTo().window(tab_handle); 
	}
	public void switchTo_nextTab()
	{
		System.out.println("\t < Navigated to Next Tav > ");
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.PAGE_DOWN);
		String tab_handle = driver.getWindowHandle();
	    driver.switchTo().window(tab_handle); 
	}
	public void scrollDown()
	{
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
	}
}
