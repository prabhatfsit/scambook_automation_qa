package com.pack.tests;




import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class complaintAgainstPhonenumber extends testBaseSetup
{
	
	@Test
	public void reporingScamAgainstCompany() throws InterruptedException   
	{
		ExcelLibrary excel = new ExcelLibrary();
		
		String phoneNumber = excel.getExcelData("ScamBook", 24, 5);
		String monetaryDamage = excel.getExcelData("ScamBook", 25, 5);
		String date = excel.getExcelData("ScamBook", 28, 5);
		String details = excel.getExcelData("ScamBook", 27, 5);
		
				
		
		
		System.out.println("Reporting Scam Against PHONE number in Home Page...");
		System.out.println("------------------------------------------------------------------------------------------------\n");
		   
		scambookPage scanpage = new scambookPage(driver);
		commonMethods comm = new commonMethods(driver);
		
		scanpage.clickonImheretoreportscamButton();  
		System.out.println();
		comm.waitFourSec();
		scanpage.fillingComplaintAgainstTextFildasPhoneNumber(phoneNumber);
		comm.scrolDown_150Pixcels();
		scanpage.clickonSelecttype();
		scanpage.selectPhonefromDropdown(); 
		comm.waitThreeSec();
		scanpage.selectacarrierDropdownChoosing_BOOSTmOBILE();
		scanpage.selectacarrierDropdownChoosing_METROpCS();
		scanpage.selectacarrierDropdownChoosing_TMOBILE();
		scanpage.selectacarrierDropdownChoosing_SPRINT();
		scanpage.selectacarrierDropdownChoosing_ROGERS();
		scanpage.selectacarrierDropdownChoosing_OTHERcARRIERS();
		scanpage.selectacarrierDropdownChoosing_VIRGINmobiles();
		scanpage.fillingonMonetaryDamagesText(monetaryDamage);
		comm.scrolup_150Pixcels();
		scanpage.fillingDatefromCalendar(date);
		scanpage.fillingonDetailsText(details);
		scanpage.clickingonSubmitButton();     
		scanpage.verifyingtheLoginPage();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n-----------------------------------------------------------------------------------------------");
		
		
	}
}
