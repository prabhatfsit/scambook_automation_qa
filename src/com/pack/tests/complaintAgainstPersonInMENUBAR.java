package com.pack.tests;

import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class complaintAgainstPersonInMENUBAR  extends testBaseSetup
{

	@Test
	public void reporingScamAgainstCompany() throws InterruptedException  
	{
		ExcelLibrary excel = new ExcelLibrary();
		
		String complaintAgainstName = excel.getExcelData("ScamBook", 24, 4);
		String userName = excel.getExcelData("ScamBook", 30, 4);
		String emailAdress = excel.getExcelData("ScamBook", 29, 4);
		String location = excel.getExcelData("ScamBook", 26, 4);
		String monetaryDamage = excel.getExcelData("ScamBook", 25, 4);
		String details = excel.getExcelData("ScamBook", 27, 4);
		
		
		System.out.println("Reporting Scam Against Person through MenuBar ...");
		System.out.println("-----------------------------------------------------------------------------------------------\n");
		commonMethods comm = new commonMethods(driver);
		
		scambookPage scampagemb = new scambookPage(driver);
		System.out.println();   
		scampagemb.clickonMenuBar();
		scampagemb.clickonReportacomplaintMENUBAR();  
		scampagemb.fillComplaintAgainstTextBoxMENUBAR(complaintAgainstName);  
		scampagemb.clickonComplaintAgainstPERSONButtonMENUBAR();
		comm.waitFourSec();
		scampagemb.fillinTheirUsernameTextMENUBAR(userName);
		scampagemb.fillinEMAILaDDRESSTextMENUBAR(emailAdress);
		scampagemb.fillinTheirLocationTextMENUBAR(location);
		scampagemb.fillinMonetaryDamageTextMENUNBAR(monetaryDamage);
		scampagemb.clickonDateButtonMENUBAR_NEW();
		scampagemb.fillinDetailsTextMENUBAR(details);
		scampagemb.clickonSubmitButtionMENUBAR();
		scampagemb.verifyingtheLoginPageMB();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n-------------------------------------------------------------------------------------------------");
		
		
		
		
		
		
		
	}
}
