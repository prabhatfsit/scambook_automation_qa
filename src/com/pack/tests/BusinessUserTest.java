package com.pack.tests;

import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.BusinessResolvePage;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class BusinessUserTest extends testBaseSetup
{
	
ExcelLibrary excel = new ExcelLibrary();
	
String fname1 = excel.getExcelData("ScamBook", 2, 1);
String fname2 = excel.getExcelData("ScamBook", 2, 2);
String fname3 = excel.getExcelData("ScamBook", 2, 3);

String lname1 = excel.getExcelData("ScamBook", 3, 1);
String lname2 = excel.getExcelData("ScamBook", 3, 2);
String lname3 = excel.getExcelData("ScamBook", 3, 3);

String compn1 = excel.getExcelData("ScamBook", 4, 1);
String compn2 = excel.getExcelData("ScamBook", 4, 2);
String compn3 = excel.getExcelData("ScamBook", 4, 3);

String compurl1 = excel.getExcelData("ScamBook", 5, 1);
String compurl2 = excel.getExcelData("ScamBook", 5, 2);
String compurl3 = excel.getExcelData("ScamBook", 5, 3);

String emailadd1 = excel.getExcelData("ScamBook", 6, 1);
String emailadd2 = excel.getExcelData("ScamBook", 6, 2);
String emailadd3 = excel.getExcelData("ScamBook", 6, 3);

String phonenum1 = excel.getExcelData("ScamBook", 7, 1);
String phonenum2 = excel.getExcelData("ScamBook", 7, 2);
String phonenum3 = excel.getExcelData("ScamBook", 7, 3);

String cardnumber = excel.getExcelData("ScamBook", 8, 1);
String cvc = excel.getExcelData("ScamBook", 9, 1);
String month = excel.getExcelData("ScamBook", 10, 1);
String yer = excel.getExcelData("ScamBook", 11, 1);
String billadd1 = excel.getExcelData("ScamBook", 12, 1);
String billadd2 = excel.getExcelData("ScamBook", 13, 2);
String billcity = excel.getExcelData("ScamBook", 15, 1);
String billzipcode = excel.getExcelData("ScamBook", 17, 1);
String billcntry = excel.getExcelData("ScamBook", 14, 1);
String billregion = excel.getExcelData("ScamBook", 16, 1);




	@Test
	public void VerifyBusinessUser() throws InterruptedException 
	{
		
		
		scambookPage bu = new scambookPage(driver);
		commonMethods comm = new commonMethods(driver);
		BusinessResolvePage brp = new BusinessResolvePage(driver);
		
		System.out.println("Positive Scenario for Business User feature for all three modes----->");
		System.out.println("--------------------------------------------------------------------------------------------------------------------------\n");
		
System.out.println("\n\n\n--------< Starter Business user >-------\n");	
System.out.println("------------------------------------------------------------------------------------------------------------\n");
		bu.click_BusinessUser_Button();
		bu.fill_firstname_Textfield(fname1);
		bu.fill_lastname_Textfield(lname1);
		bu.fill_CompanyName_Textfield(compn1);
		bu.fill_CompanyUrl_Textfield(compurl1);
		bu.fill_emailaddress_Textfield(emailadd1);
		bu.click_phonePIN_button();
		bu.fill_phonenumber_Textfield(phonenum1);
		comm.scrolDown_150Pixcels();
		bu.click_signupnowbu_button();
		bu.click_signupnowbu_button();
		bu.verify_businessResolvePage_isdisplayed();
		
		brp.verifying_contents_starter();
		comm.scrolup_150Pixcels();
		brp.click_Starter_signup_button();
		comm.scrolup_150Pixcels();
		
		brp.verifying_starter_contactinfo_firstname(fname1);
		brp.verifying_starter_contactinfo_Lastname(lname1);
		brp.verifying_starter_contactinfo_email(emailadd1);
		brp.verifying_starter_contactinfo_phonenubmer(phonenum1);
		
		brp.fill_cardholderName_textbox(fname1);
		brp.fill_creditcardnumber_textbox(cardnumber);
		brp.fill_cvc_textbox(cvc);
		brp.select_creditcard_expireMonth_textbox(month);
		brp.select_creditcard_expireYear_textbox(yer);
		
		brp.fill_billingAddressLine1_textbox(billadd1);
		brp.fill_billingAddressLine2_textbox(billadd2);
		brp.fill_billingCITY_textbox(billcity);
		brp.fill_billingZipcode_textbox(billzipcode);
		brp.select_Country_dropdown(billcntry);
		brp.fill_billingRegion_textbox(billregion);
		brp.click_Iagree_checkbox();
		brp.click_submit_button();
		comm.waitOneSec();
		brp.verify_sucess_pagedisplayed1();
		comm.waitTwoSec();
		brp.click_on_scambook_icon();
		brp.verifying_dashboard_page();
		brp.click_setting_icon();
		brp.click_logout_linK_inSettings();
		comm.waitTwoSec();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n---------------------------------------------------------------------------------------------------");
		
System.out.println("---------< standard Bussiness User >-----------");
System.out.println("------------------------------------------------------------------------------------------------\n");
		bu.click_BusinessUser_Button();
		bu.fill_firstname_Textfield(fname2);
		bu.fill_lastname_Textfield(lname2);
		bu.fill_CompanyName_Textfield(compn2);
		bu.fill_CompanyUrl_Textfield(compurl2);
		bu.fill_emailaddress_Textfield(emailadd2);
		bu.click_phonePIN_button();
		bu.fill_phonenumber_Textfield(phonenum2);
		comm.scrolDown_150Pixcels();
		bu.click_signupnowbu_button();
		bu.click_signupnowbu_button();
		bu.verify_businessResolvePage_isdisplayed();
		
		brp.verifying_contents_standard();
		comm.scrolup_150Pixcels();
		brp.click_Standard_signup_button();
		comm.scrolup_150Pixcels();
		
		brp.verifying_starter_contactinfo_firstname(fname2);
		brp.verifying_starter_contactinfo_Lastname(lname2);
		brp.verifying_starter_contactinfo_email(emailadd2);
		brp.verifying_starter_contactinfo_phonenubmer(phonenum2);
		
		brp.fill_cardholderName_textbox(fname2);
		brp.fill_creditcardnumber_textbox(cardnumber);
		brp.fill_cvc_textbox(cvc);
		brp.select_creditcard_expireMonth_textbox(month);
		brp.select_creditcard_expireYear_textbox(yer);
		
		brp.fill_billingAddressLine1_textbox(billadd1);
		brp.fill_billingAddressLine2_textbox(billadd2);
		brp.fill_billingCITY_textbox(billcity);
		brp.fill_billingZipcode_textbox(billzipcode);
		brp.select_Country_dropdown(billcntry);
		brp.fill_billingRegion_textbox(billregion);
		brp.click_Iagree_checkbox();
		brp.click_submit_button();
		comm.waitOneSec();
		brp.verify_successPage_through_url();
		comm.waitTwoSec();
		brp.click_on_scambook_icon();
		brp.verifying_dashboard_page();
		brp.click_setting_icon();
		brp.click_logout_linK_inSettings();
		comm.waitTwoSec();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("-----------------------------------------------------------------------------------------");
		
		    
System.out.println("---------< Enterprise Bussiness User >----------");	
System.out.println("------------------------------------------------------------------------------------------------\n");
		bu.click_BusinessUser_Button();
		bu.fill_firstname_Textfield(fname3);
		bu.fill_lastname_Textfield(lname3);
		bu.fill_CompanyName_Textfield(compn3);
		bu.fill_CompanyUrl_Textfield(compurl3);
		bu.fill_emailaddress_Textfield(emailadd3);
		bu.click_phonePIN_button();
		bu.fill_phonenumber_Textfield(phonenum3);
		comm.scrolDown_150Pixcels();
		bu.click_signupnowbu_button();
		bu.click_signupnowbu_button();
		bu.verify_businessResolvePage_isdisplayed();
		
		brp.verifying_contents_Enterprise();
		comm.scrolup_150Pixcels();
		brp.click_Enterprise_signup_button();
		comm.scrolup_150Pixcels();
		
		brp.verifying_starter_contactinfo_firstname(fname3);
		brp.verifying_starter_contactinfo_Lastname(lname3);
		brp.verifying_starter_contactinfo_email(emailadd3);
		brp.verifying_starter_contactinfo_phonenubmer(phonenum3);
		
		brp.fill_cardholderName_textbox(fname3);
		brp.fill_creditcardnumber_textbox(cardnumber);
		brp.fill_cvc_textbox(cvc);
		brp.select_creditcard_expireMonth_textbox(month);
		brp.select_creditcard_expireYear_textbox(yer);
		
		brp.fill_billingAddressLine1_textbox(billadd1);
		brp.fill_billingAddressLine2_textbox(billadd2);
		brp.fill_billingCITY_textbox(billcity);
		brp.fill_billingZipcode_textbox(billzipcode);
		brp.select_Country_dropdown(billcntry);
		brp.fill_billingRegion_textbox(billregion);
		brp.click_Iagree_checkbox();
		brp.click_submit_button();
		comm.waitTwoSec();
		brp.verify_successPage_through_url();
		brp.click_on_scambook_icon();
		comm.waitOneSec();
		brp.verifying_dashboard_page();
		brp.click_setting_icon();
		brp.click_logout_linK_inSettings();
		comm.waitTwoSec();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n--------------------------------------------------------------------------------------------------------------");
		
		
		
		
		
	}
}
