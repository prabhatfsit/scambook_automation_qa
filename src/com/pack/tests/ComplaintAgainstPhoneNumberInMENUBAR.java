package com.pack.tests;

import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class ComplaintAgainstPhoneNumberInMENUBAR extends testBaseSetup
{
	@Test
	public void reporingScamAgainstCompany() throws InterruptedException  
	{
		ExcelLibrary excel = new ExcelLibrary();
		
		String phoneNumber = excel.getExcelData("ScamBook", 24, 6);
		String monetaryDamage = excel.getExcelData("ScamBook", 25, 6);
		String Details = excel.getExcelData("ScamBook", 27, 6);
		
		
		    
		System.out.println("Reporting Scam Against PhoneNumber through MenuBar...");
		System.out.println("-----------------------------------------------------------------------------------------------\n");
		commonMethods comm = new commonMethods(driver);
		
		scambookPage scampagemb = new scambookPage(driver);
		
		System.out.println();
		scampagemb.clickonMenuBar();
		scampagemb.clickonReportacomplaintMENUBAR();
		scampagemb.fillComplaintAgainstTextBoxMENUBAR(phoneNumber);	
		scampagemb.clickonComplaintAgainstPHONEnUMBERButtonMENUBAR();
		comm.waitFourSec();
		scampagemb.fillinMonetaryDamageTextMENUNBAR(monetaryDamage);
		scampagemb.clickonDateButtonMENUBAR_NEW();  
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("AT&T");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("T-Mobile");  
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Sprint");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Verizon");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Metro PCS");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Boost Mobile");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Virgin Mobile");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Rogers");
		scampagemb.selectaoptionfrom_PhoneCompany_Dropdown("Other Carrier");
		scampagemb.fillinDetailsTextMENUBAR(Details);
		scampagemb.clickonSubmitButtionMENUBAR();
		scampagemb.verifyingtheLoginPageMB();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n------------------------------------------------------------------------------------------------");
		
		
		
		
	}
}
