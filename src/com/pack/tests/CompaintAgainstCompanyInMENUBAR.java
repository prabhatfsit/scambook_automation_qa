package com.pack.tests;


import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class CompaintAgainstCompanyInMENUBAR  extends testBaseSetup
{
	@Test
	public void reporingScamAgainstCompany() throws InterruptedException  
	{
		ExcelLibrary excel = new ExcelLibrary();
		
		String company =  excel.getExcelData("ScamBook", 24, 2);
		String monetaryDamage = excel.getExcelData("ScamBook", 25, 2);
		String location = excel.getExcelData("ScamBook", 26, 2);
		String details = excel.getExcelData("ScamBook", 27, 2);
		   
		
		System.out.println("\nReporting Scam Against COMPANY through MenuBar...");
		System.out.println("-----------------------------------------------------------------------------------------------\n");
		commonMethods comm= new commonMethods(driver);
		
		scambookPage scampagemb = new scambookPage(driver);
		System.out.println();
		scampagemb.clickonMenuBar();
		scampagemb.clickonReportacomplaintMENUBAR(); 
		scampagemb.fillComplaintAgainstTextBoxMENUBAR(company);  
		scampagemb.clickonComplaintAgainstCompanyButtonMENUBAR();
		comm.waitFourSec();
		scampagemb.fillinMonetaryDamageTextMENUNBAR(monetaryDamage);
		scampagemb.fillinCompanyLocationTextMENUBAR(location);
		scampagemb.fillinDetailsTextMENUBAR(details);
		scampagemb.clickonDateButtonMENUBAR_NEW();
		scampagemb.clickonSubmitButtionMENUBAR();
		scampagemb.verifyingtheLoginPageMB();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("*------------------------------------------------------------------------------------------------*");
		
		
		
		
		
		/* companys name list
		 *	- notapplicable
		 *	- ioffernote
		 *	- kamagra
		 *	- SPORTSWELCOME
		 *	- denimbasement
		 *	- fastloanfast
		 *	- everingame
		 *	- lifestyledealsnow
		 *	- bestbuywin
		 *	- moneyonthego
		 *	- monstrocloud
		 *	- walmartgifts
		 *	- tesco Petrol Station Balley Yorkshire
		 *	- tesco.org
		 *	- tesco Great Yarmouth Norfolk
		 *	- tesco car insurance
		 *	- facebook free tesco voucher worth
		 *	- tesco U.K.Bank and Red Bull energy drink
		 *	- tesco U.K Bank Red Bull
		 */
	}
		
}
