package com.pack.tests;

import java.awt.AWTException;

import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.BusinessResolvePage;
import com.pack.pageobjects.FakeMailIDGeneratorPage;
import com.pack.pageobjects.PremiumSignupPage;
import com.pack.pageobjects.SignupPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class SignupPageTest  extends testBaseSetup
{

	@Test
	public void regestrationTest() throws InterruptedException, AWTException
	{
		ExcelLibrary excel = new ExcelLibrary();
		
	//1. credentials
	 	String firstName = excel.getExcelData("ScamBook", 36, 1);
		String lastName = excel.getExcelData("ScamBook", 37, 1);
										String email = excel.getExcelData("ScamBook", 38, 1);
		String password = excel.getExcelData("ScamBook", 39, 1);
		String confirmpswd = excel.getExcelData("ScamBook", 40, 1);
		String cardholderName = excel.getExcelData("ScamBook", 41, 1);
		String creditcardNumber = excel.getExcelData("ScamBook", 42, 1);
		String month = excel.getExcelData("ScamBook", 43, 1);
		String year = excel.getExcelData("ScamBook", 44, 1);
		String cvc = excel.getExcelData("ScamBook", 45, 1);
		String address1 = excel.getExcelData("ScamBook", 46, 1);;
		String address2 = excel.getExcelData("ScamBook", 47, 1);
		String country = excel.getExcelData("ScamBook", 48, 1);
		String region  = excel.getExcelData("ScamBook", 49, 1);
		String city = excel.getExcelData("ScamBook", 50, 1);
		String zipcode = excel.getExcelData("ScamBook", 51, 1);
		String phoneNumber = excel.getExcelData("ScamBook", 52, 1);
		
	//2. credentials
									String fakemailID_MailboxName = excel.getExcelData("ScamBook", 54, 1);
		String firstName2 = excel.getExcelData("ScamBook", 55, 1);
		String lastName2 = excel.getExcelData("ScamBook", 56, 1);
		String password2 = excel.getExcelData("ScamBook", 57, 1);
		String confirmpswd2 = excel.getExcelData("ScamBook", 58, 1);
			
		
		   
	// Objects
		SignupPage reg = new SignupPage(driver);
		PremiumSignupPage preg = new PremiumSignupPage(driver);
		commonMethods comm = new commonMethods(driver);
		BusinessResolvePage brp = new BusinessResolvePage(driver);
		FakeMailIDGeneratorPage fkid = new FakeMailIDGeneratorPage(driver);
		
		
	// Scenarios
		System.out.println("\n 1. Positive Scenario for Regestration form ----->");
		System.out.println("---------------------------------------------------------------------------------------------------\n");
		reg.click_login_link();
		reg.click_signupNow_link();
		reg.fill_firstName_testBox(firstName);
		reg.fill_lastName_testBox(lastName);
		reg.fill_emailaddress_testBox(email);
		reg.fill_password_testBox(password);
		reg.fill_confirmpassword_testBox(confirmpswd);
		reg.click_Iagree_checkbox();
		reg.click_createAccount_Button();
		preg.verify_premiumpage();
		preg.fill_cardholderName_testBox(cardholderName);
		preg.fill_creditCardNumber_testBox(creditcardNumber);
		preg.select_month_Dropdown(month);
		preg.select_year_Dropdown(year);
		preg.fill_cvc_testBox(cvc);
		preg.fill_address1_testBox(address1);
		preg.fill_address2_testBox(address2);
		preg.select_Country_Dropdown(country);
		preg.fill_region_testBox(region);
		preg.fill_city_testBox(city);
		preg.fill_zipcode_testBox(zipcode);
		preg.fill_phonenumber_testBox(phoneNumber);
		preg.click_Iagree_checkbox();
		preg.click_getPremiumSupport_link();
		preg.verify_dashboardpage();
		brp.click_setting_icon();
		brp.click_logout_linK_inSettings();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n-----------------------------------------------------------------------------------------------------");
		
		
		
		System.out.println(" 2. Creating the account and Checking the confirmation link Sent to the mail ID given at Sign up process ---> ");
		System.out.println("------------------------------------------------------------------------------------------------------\n");
		
		
		String fakeID = fkid.generate_Fake_EmailID(fakemailID_MailboxName);
		//String fakeIDLink = fkid.link;
		
		comm.openNewTab();
		driver.get("http://dev.scambook.com/");
		System.out.println("\t ScamBook application opened");
		reg.click_login_link();
		reg.click_signupNow_link();
		reg.fill_firstName_testBox(firstName2);
		reg.fill_lastName_testBox(lastName2);
		reg.fill_emailaddress_testBox(fakeID);
		reg.fill_password_testBox(password2);
		reg.fill_confirmpassword_testBox(confirmpswd2);
		reg.click_Iagree_checkbox();
		reg.click_createAccount_Button();
		preg.verify_premiumpage();
		brp.click_on_scambook_icon();
		brp.verifying_dashboard_page();
		brp.click_setting_icon();
		brp.click_AccoutSetting_linK_inSettings();
		brp.verifying_dashboardAccountSetting_page();
		brp.verifying_errorMessagein_dashboardAccountsetting_page();
		comm.switchTo_previousTab();
		driver.switchTo().defaultContent();
		comm.waitOneSec();
		for(int i=0;i<8;i++)
		{comm.pressDownButton();}
		//String winHandleBefore = driver.getWindowHandle();
		fkid.clickon_scambookVerification_link();
		comm.waitOneSec();
		brp.verifying_AccountConfirmation_page();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n----------------------------------------------------------------------------------------------");
	}
		
}









