package com.pack.tests;

//import org.openqa.selenium.JavascriptExecutor;

import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class complaintAgainstPerson extends testBaseSetup                  //Person
{
	@Test
	public void reporingScamAgainstCompany() throws InterruptedException  
	{
		ExcelLibrary excel = new ExcelLibrary();
		
		String complaintAgainstName = excel.getExcelData("ScamBook", 24, 3);
		String userName = excel.getExcelData("ScamBook", 30, 3);
		String mail = excel.getExcelData("ScamBook", 29, 3);
		String location = excel.getExcelData("ScamBook", 26, 3);
		String monetaryDamage = excel.getExcelData("ScamBook", 25, 3);
		String date = excel.getExcelData("ScamBook", 28, 3);
		String details = excel.getExcelData("ScamBook", 27, 3);
		
		
		System.out.println("Reporting Scam Against PERSON in Home Page...");
		System.out.println("------------------------------------------------------------------------------------------------");
		
		scambookPage scanpage = new scambookPage(driver);
		commonMethods comm = new commonMethods(driver);    
		
		System.out.println();
		scanpage.clickonImheretoreportscamButton();
		comm.waitFourSec();
		scanpage.fillingComplaintAgainstTextFild_person(complaintAgainstName);
		scanpage.clickonSelecttype(); 
		comm.scrolDown_150Pixcels();
		scanpage.selectPersonfromDropdown();    
		scanpage.fillingTheirUserNameTextField(userName);
		scanpage.fillingTheirEmailaddressTextField(mail);
		scanpage.fillingTheirLocationTextField(location);
		scanpage.fillingonMonetaryDamagesText(monetaryDamage);
		scanpage.fillingDatefromCalendar(date);
		scanpage.fillingonDetailsText(details);
		scanpage.clickingonSubmitButton();
		scanpage.verifyingtheLoginPage();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n-----------------------------------------------------------------------------------------------");
		
		
	}
}
				