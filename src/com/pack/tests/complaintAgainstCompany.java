package com.pack.tests;

import org.testng.annotations.Test;

import com.pack.commonmethods.commonMethods;
import com.pack.pageobjects.scambookPage;
import com.pack.testbase.testBaseSetup;

import ExcelSheet.ExcelLibrary;

public class complaintAgainstCompany extends testBaseSetup                   //Company
{	
	
	@Test
	public void reporingScamAgainstCompany() throws InterruptedException  
	{
		ExcelLibrary excel = new ExcelLibrary();
		
		String company = excel.getExcelData("ScamBook", 24, 1);
		String location = excel.getExcelData("ScamBook", 26, 1);
		String monetaryDamage = excel.getExcelData("ScamBook", 25, 1);
		String details = excel.getExcelData("ScamBook", 27, 1);
		String date = excel.getExcelData("ScamBook", 28, 1);
		
		    
		
		System.out.println("\nReporting Scam Against COMPANY in Home Page...");
		System.out.println("-----------------------------------------------------------------------------------------------\n");
		
		scambookPage scanpage = new scambookPage(driver);  
		commonMethods comm = new commonMethods(driver);  
		
		System.out.println();
		scanpage.clickonImheretoreportscamButton();   
		comm.waitFourSec();  
		scanpage.fillingComplaintAgainstTextFild(company);      
		scanpage.clickonSelecttype1();     
		scanpage.selectCompanyfromDropdown();
		scanpage.clickingonCompanyLocationText(location);  
		scanpage.fillingonMonetaryDamagesText(monetaryDamage);
		scanpage.fillingonDetailsText(details);
		scanpage.fillingDatefromCalendar(date); 
		scanpage.clickingonSubmitButton();
		scanpage.verifyingtheLoginPage();
		System.out.println("<<<Scenario executed successfully>>> ");
		System.out.println("\n----------------------------------------------------------------------------------------------*");
		
		
		
		/* companys name list
		 *	- kamagra
		 *	- SPORTSWELCOME
		 *	- denimbasement
		 *	- fastloanfast
		 *	- everingame
		 *	- lifestyledealsnow
		 *	- bestbuywin
		 *	- moneyonthego
		 *	- monstrocloud
		 *	- walmartgifts
		 *	- zealmoneysolutions
		 *	- tesco Petrol Station Balley Yorkshire
		 *	- tesco.org
		 *	- tesco Great Yarmouth Norfolk
		 *	- tesco car insurance
		 *	- facebook free tesco voucher worth
		 *	- tesco U.K.Bank and Red Bull energy drink
		 *	- tesco U.K Bank Red Bull
		 */
	}
}
