package com.pack.pageobjects;

import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import com.pack.commonmethods.commonMethods;

public class BusinessResolvePage 
{
	private WebDriver driver;
	public BusinessResolvePage(WebDriver driver) {
		this.driver = driver;
	}
	
	scambookPage scm = new scambookPage(driver);	
	private By starterpoints = By.xpath("html/body/div[1]/div[4]/div/div/div/div[2]/div/div[2]/ul/li[1]/ul[2]");
	private By startersignup = By.xpath("html/body/div[1]/div[4]/div/div/div/div[2]/div/div[2]/ul/li[1]/ul[3]/li/a");
	private By standardpoints = By.xpath("html/body/div[1]/div[4]/div/div/div/div[2]/div/div[2]/ul/li[2]/ul[2]");
	private By standardsignup = By.xpath("html/body/div[1]/div[4]/div/div/div/div[2]/div/div[2]/ul/li[2]/ul[3]/li/a");
	private By enteprisepoints = By.xpath("html/body/div[1]/div[4]/div/div/div/div[2]/div/div[2]/ul/li[3]/ul[2]");
	private By enterprisesignup = By.xpath("html/body/div[1]/div[4]/div/div/div/div[2]/div/div[2]/ul/li[3]/ul[3]/li/a");
	private By logout = By.xpath("html/body/div[1]/div[3]/div/div/div/header/nav/ul/li[2]/a");
	
	private By starter_firstname = By.id("contact-first-name");
	private By starter_lastname = By.id("contact-last-name");
	private By starter_phonenumber = By.id("contact-phone");
	private By starter_emailId =By.id("contact-email");
	
	private By cardholdername = By.xpath("html/body/div[1]/div[4]/div/div/div[1]/div[2]/div/div/div[2]/form/div[1]/div[9]/div/input[1]");
	private By creditcardnubmer = By.xpath("html/body/div[1]/div[4]/div/div/div[1]/div[2]/div/div/div[2]/form/div[1]/div[9]/div/input[2]");
	private By cvc = By.xpath("html/body/div[1]/div[4]/div/div/div[1]/div[2]/div/div/div[2]/form/div[1]/div[9]/div/input[3]");
	private By expiremonth = By.xpath("html/body/div[1]/div[4]/div/div/div[1]/div[2]/div/div/div[2]/form/div[1]/div[9]/div/div[10]/select[1]");
	private By expireyear = By.xpath("html/body/div[1]/div[4]/div/div/div[1]/div[2]/div/div/div[2]/form/div[1]/div[9]/div/div[10]/select[2]");
	
	private By billingaddress1 = By.id("company-billing-address-1");
	private By billingaddress2 = By.id("company-billing-address-2");
	private By billingcity = By.id("company-billing-city");
	private By billingzipcode = By.id("company-billing-zipcode");
	private By billingcountry = By.id("company-billing-country-code");
	private By billingregion = By.id("company-billing-other-region");
	private By billingstate = By.id("company-billing-us-states");
	private By iagree = By.xpath("html/body/div[1]/div[4]/div/div/div[1]/div[2]/div/div/div[2]/form/div[1]/div[12]/input");
	private By submit = By.id("btn-next-business-signup");
	private By verifysuccess = By.xpath("html/body/div[12]/div[2]");
	
//	private By popupcancel = By.xpath("html/body/div[12]/div[2]/div[1]");
	private By close = By.className("close");
	
	commonMethods comm  = new commonMethods(driver);
	
	
//operating on starter contents
	public boolean verifying_contents_starter()
	{
		try
		{
			//System.out.println("verifying the contents in the STARTER lists ... ");
		
			WebElement starterPointsSection = driver.findElement(starterpoints);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", starterPointsSection);
			String TextInStarterSection = starterPointsSection.getText();
			System.out.println("--Contents/Points listed in the Started Section-- \n"+TextInStarterSection );
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}
	
//operatin on Starter sign up button
	public boolean click_Starter_signup_button()
	{
		try
		{
		//	System.out.println("\tclicking on Starter Sign up button ... ");
			WebElement startersignupwe = driver.findElement(startersignup);
		//	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", startersignupwe);
			Thread.sleep(500); 
			if(startersignupwe.isDisplayed()||startersignupwe.isEnabled())
			{
				startersignupwe.click();
				System.out.println("CORRECT ---> Clicked on the Starter SIGN UP button ");
			}
			else	System.out.println("INCORRECT -X-> Sign up button is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} 
		
	}			
	
	
	
	
	
	
	


//operating on standard contents
	public boolean verifying_contents_standard()
	{
		try
		{
		//	System.out.println("verifying the contents in the STANDARD lists ... ");
	
			WebElement standardPointsSection = driver.findElement(standardpoints);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", standardPointsSection);
			Thread.sleep(500);
			String TextInStandardSection = standardPointsSection.getText();
			System.out.println("-->>Contents/Points listed in the Standard Section-- \n"+TextInStandardSection );
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
//operatin on Standad sign up button
	public boolean click_Standard_signup_button()
	{
		try
		{
		//	System.out.println("\tclicking on Standard Sign up button ... ");
			WebElement standardsignupwe = driver.findElement(standardsignup);
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", standardsignupwe);
			Thread.sleep(500);
			if(standardsignupwe.isDisplayed()||standardsignupwe.isEnabled())
			{
				standardsignupwe.click();
				System.out.println("CORRECT ---> Clicked on the Standard SIGN UP button ");
			}
			else	System.out.println("INCORRECT -X--> Sign up button is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} 
		
	}				

	
	
	
	

	


//operating on Enterprise contents
	public boolean verifying_contents_Enterprise()
	{
		try
		{
			//System.out.println("verifying the contents in the Enterprise lists ... ");
		
			WebElement enterprisePointsSection = driver.findElement(enteprisepoints);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", enterprisePointsSection);
			Thread.sleep(500);
			String TextInEnterpriseSection = enterprisePointsSection.getText();
			System.out.println("-->>Contents/Points listed in the Enterprise Section-- \n"+TextInEnterpriseSection );
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}
	
//operatin on Enterprise sign up button
	public boolean click_Enterprise_signup_button()
	{
		try
		{
		//	System.out.println("clicking on Enterprise Sign up button ... ");
			WebElement enterprisesignupwe = driver.findElement(enterprisesignup);
			//((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", enterprisesignupwe);
			Thread.sleep(500);
			if(enterprisesignupwe.isDisplayed()||enterprisesignupwe.isEnabled())
			{
				enterprisesignupwe.click();
				System.out.println("CORRECT ---> Clicked on the Enterprise SIGN UP button ");
			}
			else	System.out.println("INCORRECT -X-> Sign up button is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} 
		
	}				

	
	
	
	
	
	
	
//verifying the starter contact informatioin  - First name text field
	public boolean verifying_starter_contactinfo_firstname(String fnames)
	{
		try
		{
			System.out.println("----<<verifying the contact information>>----");
		//	System.out.println("\tverifying the first name of the user in contact information section ...");
			WebElement verify = driver.findElement(starter_firstname);
			if(verify.isEnabled()||verify.isDisplayed())
			{
				String display = verify.getAttribute("value");
				if(fnames.equals(display))
					System.out.println("CORRECT ---> Data in the First name text field is -<"+fnames+">-matches the date entered in the home page");
				else
					System.out.println("INCORRECT -X--> Data in the First name text field DO NOT matches the date entered in the home page");
			}
			else
				System.out.println("INCORRECT -X-> First name text field in the contact information section is NOT foud ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}
	
	
//verifying the starter contact informatioin   - Last name text field
	public boolean verifying_starter_contactinfo_Lastname(String lnames)
	{
		try
		{
			//System.out.println("\tverifying the Last name of the user in contact information section ...");
			WebElement verify = driver.findElement(starter_lastname);
			if(verify.isEnabled()||verify.isDisplayed())
			{
				String display = verify.getAttribute("value");
				if(lnames.equals(display))
					System.out.println("CORRECT ---> Data in the Last name text field is -<"+display+">- and it Matches the date entered in the home page");
				else
					System.out.println("INCORRECT -X-> Data in the Last name text field DO NOT matches the date entered in the home page");
			}
			else
				System.out.println("INCORRECT -X-> First name text field in the contact information section is NOT foud ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}	
	
//verifying the starter contact informatioin   -phone number text field
	public boolean verifying_starter_contactinfo_phonenubmer(String phones)
	{
		try
		{
		//	System.out.println("\tverifying the Phone number of the user in contact information section ...");
			WebElement verify = driver.findElement(starter_phonenumber);
			if(verify.isEnabled()||verify.isDisplayed())
			{
				String display = verify.getAttribute("value");
				if(phones.equals(display))
					System.out.println("CORRECT ---> Data in the Last name text field is -<"+display+">- and it Matches the date entered in the home page");
				else
					System.out.println("INCORRECT -X-> Data in the Last name text field DO NOT matches the date entered in the home page");
			}
			else
				System.out.println("INCORRECT -X-> First name text field in the contact information section is NOT foud ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}		
	

//verifying the starter contact informatioin   - email text field
	public boolean verifying_starter_contactinfo_email(String emails)
	{
		try
		{
			//System.out.println("\tverifying the Email of the user in contact information section ...");
			WebElement verify = driver.findElement(starter_emailId);
			if(verify.isEnabled()||verify.isDisplayed())
			{
				String display = verify.getAttribute("value");
				if(emails.equals(display))
					System.out.println("CORRECT ---> Data in the Email text field is -<"+display+">- and it Matches the date entered in the home page");
				else
					System.out.println("INCORRECT -X-> Data in the Last name text field DO NOT matches the date entered in the home page");
			}
			else
				System.out.println("INCORRECT -X-> First name text field in the contact information section is NOT foud ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}		
		
	
	
	
	

//verifying the starter CREDIT CARD  informatioin   - Card holder name text field
	public boolean fill_cardholderName_textbox(String cname)
	{
		try
		{
		//	System.out.println("----<Filling credit card informations>----");
			System.out.println("\tEntering the cardholder name ... ");
			WebElement cardholdernamewe = driver.findElement(cardholdername);
			if(cardholdernamewe.isEnabled()||cardholdernamewe.isDisplayed())
			{
				cardholdernamewe.sendKeys(cname);
					System.out.println("CORRECT ---> Cardholder name entered as --------<"+cname+">");
			}
			else
				System.out.println("INCORRECT -X-> Cardholder name text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}		
		
	

//verifying the starter CREDIT CARD  informatioin   - Credit Card number name text field
	public boolean fill_creditcardnumber_textbox(String cnumber)
	{
		try
		{
			//System.out.println("\tEntering the Credit cardnumber  ... ");
			WebElement creditcardnubmerwe = driver.findElement(creditcardnubmer);
			if(creditcardnubmerwe.isEnabled()||creditcardnubmerwe.isDisplayed())
			{
				creditcardnubmerwe.sendKeys(cnumber); 
					System.out.println("CORRECT ---> Credit cardnumber entered as ------<"+cnumber+">");
			}
			else
				System.out.println("INCORRECT -X-> Credit Card number text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}			
	
	


//verifying the starter CREDIT CARD  informatioin   - Credit Card cvc number  text field
	public boolean fill_cvc_textbox(String cvcnum)
	{
		try
		{
		//	System.out.println("\tEntering the Credit card cvc number  ... ");
			WebElement cvcwe = driver.findElement(cvc);
			if(cvcwe.isEnabled()||cvcwe.isDisplayed())
			{
				cvcwe.sendKeys(cvcnum); 
					System.out.println("CORRECT ---> Credit card cvc number entered as -<"+cvcnum+">");
			}
			else
				System.out.println("INCORRECT -X-> Credit Card cvc number text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}			
		
	
	
//verifying the starter CREDIT CARD  informatioin   - Credit Card expire month dropdown
	public boolean select_creditcard_expireMonth_textbox(String emonth)
	{
		try
		{
			//System.out.println("\tSelecting the Month from Credit card expire month Dropdown  ... ");
			WebElement datecheck = driver.findElement(expiremonth);
			if(datecheck.isEnabled()||datecheck.isDisplayed())
			{
				Select mnth = new Select(datecheck);
				List<WebElement> all = mnth.getOptions();
				System.out.println("Datas in Month Dropdown ->");
				for(int i=0;i<all.size();i++)
				{
					WebElement opt = all.get(i);
					String txt = opt.getText();
					System.out.println("\t -> "+txt);
				}
				mnth.selectByValue(emonth);
				System.out.println("CORRECT ---> Credit card expire month selected as -<"+emonth+">");
			}
			else
				System.out.println("INCORRECT -X-> Credit card expire month dropdown is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}

	
	
////verifying the starter CREDIT CARD  informatioin   - Credit Card Expire year dropdown
	public boolean select_creditcard_expireYear_textbox(String yer)
	{
		try
		{
		//	System.out.println("\tselecting the Year from Credit card expire Year Dropdown  ... ");
			WebElement yearwe = driver.findElement(expireyear);
			if(yearwe.isEnabled()||yearwe.isDisplayed())
			{
				Select yr = new Select(yearwe);
				List<WebElement> all = yr.getOptions();
				System.out.println("\tDatas in Year Dropdown ->");
				for(int i=0;i<all.size();i++)
				{
					WebElement opt = all.get(i);
					String txt = opt.getText();
					System.out.println("\t -> "+txt);
				}
				yr.selectByValue(yer);
				System.out.println("CORRECT ---> Credit card expire Year selected as -<"+yer+">");
			}
			else
				System.out.println("INCORRECT -X-> Credit card expire Year dropdown is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
	
	

//operating on Billing Address   -Billing Address Line1 text field
	public boolean fill_billingAddressLine1_textbox(String billname)
	{
		try
		{
			System.out.println("----<Filling Billing Address informations>----");
		//	System.out.println("Entering the Billing address Line1  ... ");
			WebElement billingaddress1we = driver.findElement(billingaddress1);
			if(billingaddress1we.isEnabled()||billingaddress1we.isDisplayed())
			{
				billingaddress1we.sendKeys(billname);
					System.out.println("CORRECT ---> Billing address Line 1 is entered as --------<"+billname+">");
			}
			else
				System.out.println("INCORRECT -X-> Billing address Line 1 text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}			
	
	

//operating on Billing Address   -Billing Address Line2 text field
	public boolean fill_billingAddressLine2_textbox(String billname)
	{
		try
		{
		//	System.out.println("\tEntering the Billing address Line2  ... ");
			WebElement billingaddress2we = driver.findElement(billingaddress2);
			if(billingaddress2we.isEnabled()||billingaddress2we.isDisplayed())
			{
				billingaddress2we.sendKeys(billname);
					System.out.println("CORRECT ---> Billing address Line 2 is entered as --------<"+billname+">");
			}
			else
				System.out.println("INCORRECT -X-> Billing address Line 2 text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}			
			
	


//operating on Billing Address   -Billing city text field
	public boolean fill_billingCITY_textbox(String billcity)
	{
		try
		{
		//	System.out.println("\tEntering the Billing City ... ");
			WebElement billingcitywe = driver.findElement(billingcity);
			if(billingcitywe.isEnabled()||billingcitywe.isDisplayed())
			{
				billingcitywe.sendKeys(billcity);
					System.out.println("CORRECT ---> Billing City is entered as --------<"+billcity+">");
			}
			else
				System.out.println("INCORRECT --X-> Billing City text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}			
	
	

//operating on Billing Address   -Billing zipcode text field
	public boolean fill_billingZipcode_textbox(String billcity)
	{
		try
		{
		//	System.out.println("\tEntering the Billing City ... ");
			WebElement billingzipcodewe = driver.findElement(billingzipcode);
			if(billingzipcodewe.isEnabled()||billingzipcodewe.isDisplayed())
			{
				billingzipcodewe.sendKeys(billcity);
					System.out.println("CORRECT ---> Billing Zipcode is entered as --------<"+billcity+">");
			}
			else
				System.out.println("INCORRECT --X-> Billing Zipcode text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}				
	
	

//operating on Billing Address   -Selecting country form the Dropdown
	public boolean select_Country_dropdown(String cntry)
	{
		try
		{
		//	System.out.println("\tSelecting the Country from Country checkbox .... ");
			WebElement billingcountrywe = driver.findElement(billingcountry);
			if(billingcountrywe.isEnabled()||billingcountrywe.isDisplayed())
			{
				Select yr = new Select(billingcountrywe);
				List<WebElement> all = yr.getOptions();
				System.out.println("\tDatas in Country Dropdown ->");
				int count = all.size();
				System.out.println("-> "+count+" countries is there in the list");
				yr.selectByVisibleText(cntry);
				System.out.println("CORRECT ---> Country -<"+cntry+">- is selected");
			}
			else
				System.out.println("INCORRECT -X-> Country dropdown is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}				
	
	

//operating on Billing Address   -Billing Region text field
	public boolean fill_billingRegion_textbox(String billcity)
	{
		try
		{
		//	System.out.println("\tEntering the Billing Region ... ");
			WebElement billingregionwe = driver.findElement(billingregion);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", billingregionwe);
			Thread.sleep(500); 
			if(billingregionwe.isEnabled()||billingregionwe.isDisplayed())
			{
				billingregionwe.sendKeys(billcity);
					System.out.println("CORRECT ---> Billing Region is entered as --------<"+billcity+">");
			}
			else
				System.out.println("INCORRECT --X-> Billing Region text field is not found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} 
	}					
	


//operating on Billing Address   - selecting state from state dropdown
	public boolean select_state_dropdown(String sta)
	{
		try
		{
		//	System.out.println("\tSelecting the State from state Dropdown .... ");
			WebElement statewe = driver.findElement(billingstate);
			if(statewe.isEnabled()||statewe.isDisplayed())
			{
				Select ste = new Select(statewe);
				List<WebElement> all = ste.getOptions();
				System.out.println("\tDatas in Year Checkbox ->");
				for(int i=0;i<all.size();i++)
				{
					WebElement opt = all.get(i);
					String txt = opt.getText();
					System.out.println("\t -> "+txt);
				}
				ste.selectByVisibleText(sta);
				System.out.println("CORRECT ---> State "+sta+" is selected");
			}
			else
				System.out.println("INCORRECT -X-> State dropdown is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		


	
	


//operating on Billing Address   - i agree checkbox
	public boolean click_Iagree_checkbox()
	{
		try
		{
		//	System.out.println("\tClickin on I agree CheckBox ....");
			WebElement iagreewe = driver.findElement(iagree);
			if(iagreewe.isEnabled()||iagreewe.isDisplayed())
			{
				iagreewe.click();
				boolean chk = iagreewe.isSelected();
				if(chk)
					System.out.println("CORRECT ---> Iagree Check box is Ticked ");
				else
					System.out.println("INCORRECT -X-> Iagree check box is not Ticked");
			}
			else
				System.out.println("INCORRECT -X-> Iagree Check box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}				
	
	



//operating on Billing Address   - submit button
	public boolean click_submit_button()
	{
		try
		{
		//	System.out.println("\tClicking on submit button ....");
			WebElement loginlink = driver.findElement(submit);
			if(loginlink.isEnabled()||loginlink.isDisplayed())
			{
				loginlink.click();
				System.out.println("CORRECT ---> submit button is clicked ");
			}
			else
				System.out.println("INCORRECT -X-> submit button is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
	

// operatin on logout link
public boolean click_logout_button()
{
	try
	{
	//	System.out.println("\tClicking on submit button ....");
		WebElement loginlink = driver.findElement(logout);
		if(loginlink.isEnabled()||loginlink.isDisplayed())
		{
			loginlink.click();
			System.out.println("CORRECT ---> logout button is clicked ");
		}
		else
			System.out.println("INCORRECT -X-> logout button is NOT found");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}		

	

//operatin on logout link
public boolean verify_paymentSuccess_pagedisplayed()
{
	try
	{
		System.out.println("\tVerifying the page indicating - payment successfull is displayed or not ...");
		WebElement verifysuccesswe = driver.findElement(verifysuccess);
		String check = verifysuccesswe.getText();
		if(verifysuccesswe.isEnabled()||verifysuccesswe.isDisplayed())
		{
			verifysuccesswe.click();
			System.out.println("CORRECT --> Account Sign up is a success and displayed a message indicating -<<"+check+">>-");
		}
		else
			System.out.println("INCORRECT -X-> Message indicating Account sign up success is NOT found ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}		


public boolean verify_successPage_through_url()
{
	try
	{
		System.out.println("\tverifyint the page indicating - payment successful is displayed or not ...");
		String actual_url = driver.getCurrentUrl();
		String Expected_url = "http://dev.scambook.com/business/signup_success";
		if(actual_url.equals(Expected_url))
		{
			System.out.println("CORRECT ---> Payment is success and the page indicating the same is displayed ");
		}
		else
			System.out.println(" INCORRECT -X--> Payment is UNDONE and the page indicating the same is NOT displayed ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}
	


public boolean click_popup_cancel_icon()
{
	try
	{
		System.out.println("\tcancelling the notification popup ...");
		WebElement cancel = driver.findElement(close);
		if(cancel.isEnabled()||cancel.isDisplayed())
		{
			cancel.click();
			System.out.println("CORRECT ---> POPUP is canceled");
		}
		else
			System.out.println("INCORRECT -X-> POPUP is NOT found ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}
	
private By scambook_icon = By.xpath("html/body/div[1]/div[3]/div/div/div/header/div/a/img");
public boolean click_on_scambook_icon()
{
	try
	{
		System.out.println("\tclicking on the scambook icon ...");
		WebElement icon = driver.findElement(scambook_icon);
		if(icon.isEnabled()||icon.isDisplayed())
		{
			icon.click();
			System.out.println("CORRECT ---> Scambook icon is clicked ");
		}
		else
			System.out.println("INCORRECT -X-> Scambook is NOT found");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}
	
public boolean verifying_dashboard_page()
{
	try
	{
		System.out.println("\tverifying the display of dashboard page ...");
		String Actual = driver.getCurrentUrl();
		String Expected = "http://dev.scambook.com/dashboard";
		if(Actual.equals(Expected))
			System.out.println("CORRECT ---> Dashboard page is displayed");
		else
			System.out.println("INCORRECT -X-> Dashboard page is NOT displayed");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}


private By setting_icon = By.xpath("html/body/header/div[1]/div[2]/div/div/div[2]/div/div[2]");
public boolean click_setting_icon()
{
	try
	{
		//System.out.println("\tclicking on setting icon ...");
		WebElement cancel = driver.findElement(setting_icon);
		if(cancel.isEnabled()||cancel.isDisplayed())
		{
			cancel.click();
			System.out.println("CORRECT ---> Setting icon is clicked");
		}
		else
			System.out.println("INCORRECT -X-> Setting icon is NOT found nor clicked ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}





private By accountSetting_link = By.xpath("html/body/header/div[1]/div[2]/div/div/div[2]/menu/li[1]/a");
public boolean click_AccoutSetting_linK_inSettings()
{
	try
	{
		//System.out.println("\tclicking on Account Setting link in setting ...");
		WebElement accountsetng = driver.findElement(accountSetting_link);
		if(accountsetng.isEnabled()||accountsetng.isDisplayed())
		{
			accountsetng.click();
			System.out.println("CORRECT ---> Account Setting link is clicked ");
		}
		else
			System.out.println("INCORRECT -X->  Account Setting link is NOT found ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}


private By logout_link = By.xpath("html/body/header/div[1]/div[2]/div/div/div[2]/menu/li[5]/a");
public boolean click_logout_linK_inSettings()
{
	try
	{
	//	System.out.println("\tclicking on logout link in setting ...");
		WebElement logout = driver.findElement(logout_link);
		if(logout.isEnabled()||logout.isDisplayed())
		{
			logout.click();
			System.out.println("CORRECT ---> Logout link is clicked ");
		}
		else
			System.out.println("INCORRECT -X-> Logout link is NOT found ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}


private By sucess = By.xpath("html/body/div[1]/div[4]/div/div[1]/div/div[1]/div/ul/li[3][contains(@class,'active')]");
public boolean verify_sucess_pagedisplayed1()
{
	try
	{
		System.out.println("\tVerifying the page indicating - payment successfull is displayed or not ...");
		WebElement verifysuccesswe = driver.findElement(sucess);
		if(verifysuccesswe.isEnabled()||verifysuccesswe.isDisplayed())
		{
			System.out.println("CORRECT ---> Account Sign up is a success page is  displayed  and the sub tab SUCCESS is active");
		}
		else
			System.out.println("INCORRECT -X-> Message indicating Account sign up success is NOT found ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}		
	
	
	
public boolean verifying_dashboardAccountSetting_page()
{
	try
	{
		String expectedtitle = "Dashboard - Account Edit";
		String actualtitle = driver.getTitle();
		if(actualtitle.equals(expectedtitle))
			System.out.println("CORRECT ---> Dashboard Account Edit page is Displayed ");
		else
			System.out.println("INCORRECT -X-> Dashboard Account Edit page is NOT Displayed");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}
	

private By errormessage = By.xpath("html/body/div[1]/div[2]/div[2]/h3");
public boolean verifying_errorMessagein_dashboardAccountsetting_page()
{
	try
	{
		WebElement errormess = driver.findElement(errormessage);
		if(errormess.isEnabled()||errormess.isDisplayed())
		{
			String text = errormess.getText();
			System.out.println("CORRECT ---> ERROR Message is displayed and it states <<"+text+">>");
		}
		else
			System.out.println("INCORRECT -X-> Error Message is NOT found ");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		}
}
	
	
public boolean verifying_AccountConfirmation_page()
{
	try
	{

		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		driver.switchTo().defaultContent();
		comm.waitOneSec();
		
		driver.switchTo().defaultContent();
		String url = driver.getCurrentUrl();
		driver.close();

		WebDriver driver2 = new FirefoxDriver();
		driver2.get(url);
		
		String expectedTitle ="Account Confirmation";
		String actualTitle = driver2.getTitle();
		
		if(actualTitle.contains(expectedTitle))
			System.out.println("CORRECT ---> Account confirmation page is displayed ");
		else
			System.out.println("INCORRECT -X-> Account confirmation page is NOT displayed");
		return true;
	}
	catch (NoSuchElementException ignored){
		return false;
		} catch (InterruptedException e) {
		e.printStackTrace();
		return false;
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
