package com.pack.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PremiumSignupPage 
{

	private WebDriver driver;
	
	private By cardholdername = By.id("card_name");
	private By creditcardnumber = By.id("card_number");
	private By month = By.id("card_month");
	private By year = By.id("card_year");
	private By cvc = By.id("card_cvc");
	private By address1 = By.id("address1");
	private By address2 = By.id("address2");
	private By state = By.id("us_states");
	private By otherregion = By.id("other_region");
	private By city = By.id("city");
	private By zipcode = By.id("zipcode");
	private By country = By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/form/div/ul/li[7]/div/select");
	//private By phonecode = By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/form/div/ul/li[7]/div/select");
	private By phonenumber = By.id("phone_number");
	private By iagree = By.id("agree");
	private By getpremiumsupport = By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/form/div/input");
	
			
	
	
	
	public PremiumSignupPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	
	
//operation on premium page 
	public boolean verify_premiumpage()
	{
		try
		{
			String expectedtitle = "Premium Signup | Scambook";
			String actualtitle = driver.getTitle();
			if(actualtitle.equals(expectedtitle))
				System.out.println("CORRECT --> Premium Signup page is displayed ");
			else
				System.out.println("INCORRECT -X-> Premium Signup page is NOT displayed ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
//opertaion on Cardholder Name in PremiumSignup page
	public boolean fill_cardholderName_testBox(String cardhName)
	{
		try
		{
		//	System.out.println("\tFilling Cardholder Name text box ....");
			WebElement Cardholderwe = driver.findElement(cardholdername);
			if(Cardholderwe.isEnabled()||Cardholderwe.isDisplayed())
			{
				Cardholderwe.sendKeys(cardhName);
				System.out.println("CORRECT ---> CardholderName Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> CardholderName Text Box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
	
	
//opertaion on Credit card number in PremiumSignup page
	public boolean fill_creditCardNumber_testBox(String cardNum)
	{
		try
		{
			//System.out.println("\tFilling Credit card Name text box ....");
			WebElement creditcardnumberwe = driver.findElement(creditcardnumber);
			if(creditcardnumberwe.isEnabled()||creditcardnumberwe.isDisplayed())
			{
				creditcardnumberwe.sendKeys(cardNum);
				System.out.println("CORRECT ---> Credit card Number Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> Credit card Number text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}	
	
	
	
//operatioin on Month dropdown in PremiumSignup Page
	public boolean select_month_Dropdown(String mnt)
	{
		try
		{
			//System.out.println("\tSelecting the month from Month Dropdown .... ");
			WebElement datecheck = driver.findElement(month);
			if(datecheck.isEnabled()||datecheck.isDisplayed())
			{
				Select mnth = new Select(datecheck);
				List<WebElement> all = mnth.getOptions();
				System.out.println("Datas in Month Dropdown ->");
				for(int i=0;i<all.size();i++)
				{
					WebElement opt = all.get(i);
					String txt = opt.getText();
					System.out.println("\t-"+txt);
				}
				mnth.selectByValue(mnt);
				System.out.println("CORRECT --->  "+mnt+" is selected Month dropdown");
			}
			else
				System.out.println("INCORRECT -x-> Month checkbox is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
	
	
	
//operatioin on Year dropdown in PremiumSignup Page
	public boolean select_year_Dropdown(String yer)
	{
		try
		{
			//System.out.println("\tSelecting the Year from Year Dropdown .... ");
			WebElement yearwe = driver.findElement(year);
			if(yearwe.isEnabled()||yearwe.isDisplayed())
			{
				Select yr = new Select(yearwe);
				List<WebElement> all = yr.getOptions();
				System.out.println("Datas in Year Dropdown ->");
				for(int i=0;i<all.size();i++)
				{
					WebElement opt = all.get(i);
					String txt = opt.getText();
					System.out.println("\t- "+txt);
				}
				yr.selectByValue(yer);
				System.out.println("CORRECT ---> "+yer+" is selected from Year Dropdown");
			}
			else
				System.out.println("INCORRECT -x-> Year Dropdown is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}	
	
	
	
//opertaion on cvc in PremiumSignup page
	public boolean fill_cvc_testBox(String cvcno)
	{
		try
		{
			//System.out.println("\tFilling CVC text box ....");
			WebElement cvcwe = driver.findElement(cvc);
			if(cvcwe.isEnabled()||cvcwe.isDisplayed())
			{
				cvcwe.sendKeys(cvcno);
				System.out.println("CORRECT ---> CVC Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> CVC text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
//opertaion on address1 in PremiumSignup page
	public boolean fill_address1_testBox(String add1)
	{
		try
		{
			//System.out.println("\tFilling address1 text box ....");
			WebElement address1we = driver.findElement(address1);
			if(address1we.isEnabled()||address1we.isDisplayed())
			{
				address1we.sendKeys(add1);
				System.out.println("CORRECT ---> address1 Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> address1 text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}			
	
	
	
//opertaion on address2 in PremiumSignup page
	public boolean fill_address2_testBox(String add2)
	{
		try
		{
			//System.out.println("\tFilling address2 text box ....");
			WebElement address2we = driver.findElement(address2);
			if(address2we.isEnabled()||address2we.isDisplayed())
			{
				address2we.sendKeys(add2);
				System.out.println("CORRECT ---> address2 Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> address2 text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}			
	
	
	
	
//opertaion on region in PremiumSignup page
	public boolean fill_region_testBox(String othrreg)
	{
		try
		{
			//System.out.println("\tFilling region text box ....");
			WebElement otherregionwe = driver.findElement(otherregion);
			if(otherregionwe.isEnabled()||otherregionwe.isDisplayed())
			{
				otherregionwe.sendKeys(othrreg);
				System.out.println("CORRECT ---> region Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> region text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}			

	
//operatioin on state dropdown in PremiumSignup Page
	public boolean select_state_dropdown(String sta)
	{
		try
		{
			//System.out.println("\tSelecting the State from State Dropdown .... ");
			WebElement statewe = driver.findElement(state);
			if(statewe.isEnabled()||statewe.isDisplayed())
			{
				Select ste = new Select(statewe);
				List<WebElement> all = ste.getOptions();
				System.out.println("Datas in Year Checkbox ->");
				for(int i=0;i<all.size();i++)
				{
					WebElement opt = all.get(i);
					String txt = opt.getText();
					System.out.println("\t- "+txt);
				}
				ste.selectByValue(sta);
				System.out.println("CORRECT ---> State "+sta+" is selected");
			}
			else
				System.out.println("INCORRECT -x-> State dropdown is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
//operatioin on Country dropdown in PremiumSignup Page
	public boolean select_Country_Dropdown(String cntry)
	{
		try
		{
			//System.out.println("\tSelecting the Country from Country Dropdown .... ");
			WebElement yearwe = driver.findElement(country);
			if(yearwe.isEnabled()||yearwe.isDisplayed())
			{
				Select yr = new Select(yearwe);
				List<WebElement> all = yr.getOptions();
				System.out.println("Datas in Country Checkbox ->");
				int count = all.size();
				System.out.println("\t -"+count+" countries is there in the list");
				yr.selectByValue(cntry);
				System.out.println("CORRECT ---> Country "+cntry+" is selected");
			}
			else
				System.out.println("INCORRECT -x-> Country "+cntry+" is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
	
	
//opertaion on city in PremiumSignup page
	public boolean fill_city_testBox(String cty)
	{
		try
		{
			//System.out.println("\tFilling city text box ....");
			WebElement citytwe = driver.findElement(city);
			if(citytwe.isEnabled()||citytwe.isDisplayed())
			{
				citytwe.sendKeys(cty);
				System.out.println("CORRECT ---> city Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> city text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}			
	
	
	
	

//opertaion on zipcode in PremiumSignup page
	public boolean fill_zipcode_testBox(String zpcd)
	{
		try
		{
			//System.out.println("\tFilling zipcode text box ....");
			WebElement zipcodewe = driver.findElement(zipcode);
			if(zipcodewe.isEnabled()||zipcodewe.isDisplayed())
			{
				zipcodewe.sendKeys(zpcd);
				System.out.println("CORRECT ---> zipcode Text box is filled ");
			}
			else
				System.out.println("INCORRECT -x-> zipcode text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}			
	
	
	

//opertaion on phonenumber in PremiumSignup page
	public boolean fill_phonenumber_testBox(String phnno)
	{
		try
		{
			//System.out.println("\tFilling phonenumber text box ....");
			WebElement phonenumberwe = driver.findElement(phonenumber);
			if(phonenumberwe.isEnabled()||phonenumberwe.isDisplayed())
			{
				phonenumberwe.sendKeys(phnno);
				System.out.println("CORRECT ---> phonenumber Text box is filled ");
			}
			else
				System.out.println("INCORRECT --> phonenumber text box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
	

//operation on confirmpassword text box in premiumSignup page
	public boolean click_Iagree_checkbox()
	{
		try
		{
			//System.out.println("\tClickin on I agree CheckBox ....");
			WebElement iagreewe = driver.findElement(iagree);
			if(iagreewe.isEnabled()||iagreewe.isDisplayed())
			{
				iagreewe.click();
				boolean chk = iagreewe.isSelected();
				if(chk)
					System.out.println("CORRECT ---> Iagree Check box is Ticked ");
				else
					System.out.println("INCORRECT -x-> Iagree check box is not Ticked");
			}
			else
				System.out.println("INCORRECT -x-> Iagree Check box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}			
	
	
	
	
//operation on get premium support button in premiumSignup page
	public boolean click_getPremiumSupport_link()
	{
		try
		{
			//System.out.println("\tClicking on get premium support button ....");
			WebElement loginlink = driver.findElement(getpremiumsupport);
			if(loginlink.isEnabled()||loginlink.isDisplayed())
			{
				loginlink.click();
				System.out.println("CORRECT --->get premium support button is clicked ");
			}
			else
				System.out.println("INCORRECT -x-> get premium support button is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}	
	
	public boolean verify_dashboardpage()
	{
		try
		{
			//System.out.println("\tverifying the Dashboard-Account edit page is displayed or not ...");
			String expected ="Dashboard - Account Edit";
			String actual = driver.getTitle();
			if(actual.equals(expected))
				System.out.println("CORRECT ---> Dashboard - Account Edit page is displayed ");
			else
				System.out.println("INCORRECT -X-> Dashboard - Account Edit page is NOT displayed ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
