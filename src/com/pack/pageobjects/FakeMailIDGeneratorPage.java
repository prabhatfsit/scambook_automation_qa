package com.pack.pageobjects;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.interactions.Actions;

import com.pack.commonmethods.commonMethods;

public class FakeMailIDGeneratorPage
{
	private WebDriver driver;
	
	public FakeMailIDGeneratorPage(WebDriver driver)
	{
		this.driver = driver;
	}
	public String link;	
	private By mailboxName = By.id("home-email"); 	
	private By domain = By.id("domain");
	private By copy = By.id("copy-button");
	private By scamlink = By.xpath("//a[contains(@href,'dev.scambook')]");
	
	//a[contains(@href,'dev.scambook.com')]
	commonMethods comm = new commonMethods(driver);
	
	
		public String generate_Fake_EmailID(String mailbox) throws InterruptedException
		{
			String fakeid="";
			try
			{
				driver.get("http://www.fakemailgenerator.com/#/gustr.com/thatualle1973/");
				String expected = "Fake Mail Generator";
				String actual = driver.getTitle();
				if(actual.contains(expected))
					System.out.println("CORRECT ---> Fake mail id generator page is displayed");
				else
					System.out.println("INCORRECT -X-> Fake mail id generator page is NOT displayed ");
				//hostname
				WebElement mailboxNameWE = driver.findElement(mailboxName);
				if(mailboxNameWE.isDisplayed()||mailboxNameWE.isEnabled())
				{
					mailboxNameWE.clear();
					mailboxNameWE.sendKeys(mailbox);
					System.out.println("CORRECT ---> mailbox name is entered as <"+mailbox+"> in the MailBox textbox");
				}
				else
					System.out.println("INCORRECT -X-> maail box name is NOT entered ");
				//domain
				WebElement domainWE = driver.findElement(domain);
				if(domainWE.isDisplayed()||domainWE.isEnabled())
				{
					String domaintext = domainWE.getText();
					fakeid = mailbox+domaintext;
					System.out.println("CORRECT ---> Fake email id is genarated as <<"+fakeid+">>");
				}
				else 
					System.out.println("INCORRECT -X-> Domain text NOT found ");
				//copy button
				WebElement copyWE = driver.findElement(copy);
				if(copyWE.isDisplayed()||copyWE.isEnabled())
				{
					copyWE.click();
					System.out.println("CORRECT ---> COPY button is clicked ");
				}
				else
					System.out.println("INCORRECT -X-> copy button is NOT found");
				//copy link
				comm.waitOneSec();

				link = driver.getCurrentUrl();
			}
			catch (NoSuchElementException ignored){
				System.out.println("Exception found");
				}
			return fakeid;
			
		}
		
		
		public boolean clickon_scambookVerification_link() throws InterruptedException
		{
			try
			{
				System.out.println("\t     Navigating to mail ...");
				driver.switchTo().defaultContent();
				driver.switchTo().frame(1);
				comm.waitTwoSec();
				WebElement linkdev = driver.findElement(scamlink);
				if(linkdev.isDisplayed()||linkdev.isEnabled())
				{
					System.out.println("CORRECT ---> Verification link is SUCCESSFULLY sent to the provided mail ID");
					linkdev.click();
					System.out.println("CORRECT ---> Verification link of Scambook in mail id is clicked ");
				}
				else
					System.out.println("INCORRECT -X-> Verification link of Scambook is NOT found ");
				return true;
			}
			catch (NoSuchElementException ignored){
				return false;
				}
		}
	


		public boolean clickon_scambookVerification1_link()
		{
			try
			{
				System.out.println("\tNavigating to mail ...");
				driver.switchTo().defaultContent();
				WebElement linkdev = driver.findElement(scamlink);
				if(linkdev.isDisplayed()||linkdev.isEnabled())
				{
					System.out.println("CORRECT ---> Verificatioin link is present in the mail id sent by the scam book application");
					String linktext = linkdev.getText();
					driver.get(linktext);
					System.out.println("CORRECT ---> Verification link of Scambook in mail id is clicked ");
				}
				else
					System.out.println("INCORRECT -X-> Verification link of Scambook is NOT found ");
				return true;
			}
			catch (NoSuchElementException ignored){
				return false;
				}
		}
}
