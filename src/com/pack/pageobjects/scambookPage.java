package com.pack.pageobjects;


import java.util.List;

import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.pack.commonmethods.commonMethods;

//import com.pack.tests.JavascriptExector;

import ExcelSheet.ExcelLibrary;

public class scambookPage 
{
	private WebDriver driver;
	public int count = 4567;

	commonMethods comm= new commonMethods(driver);
	
public String fname;
public String lname;
public String email;
public String phone;

	private By imheretoreportascam = By.xpath("html/body/div[2]/div[1]/div[1]/div/div/ul/li[1]/a");
	private By complaintform = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/h3");
	private By complaintagainst = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[1]/div/div/div[2]/div/label/input");
	private By complaintagainstclass = By.xpath("//li/a[@class='ui-corner-all']");
	private By complaintagainsttabindex = By.xpath("//li/a[@tabindex='-1']");  
	private By selecttype = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[1]/div/div/div[2]/div/div");
	private By selecttype1 = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[1]/div/div/div[2]/div/div/span");
	private By selectcompany= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[1]/div/div/div[2]/div/div/ul/li[2]");
	private By selectperson= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[1]/div/div/div[2]/div/div/ul/li[3]");
	private By selectphone= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[1]/div/div/div[2]/div/div/ul/li[4]");
	private By companylocaton= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[2]/div/label/div/input");
	private By monetarydamages= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div[1]/label/div/input");
	private By details= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div[3]/label/textarea");
	private By datebutton = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div[2]/div/label/input");
	private By submit = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div[4]/div/input");
	private By createAccount =By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[3]/div/div[4]/div/input");
	private By createAccountMB =By.xpath("//div[2]/span[contains(text(),'Create Account')]");
	private By username = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[1]/div[1]/div[1]/label/input");
	private By emailaddress = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[1]/div[1]/div[2]/label/input");
	private By theirlocation = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[1]/div[2]/div/label/input");
	private By selectcarrier = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[1]/div/label/select");
	private By back = By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[2]/div/span/a");
	private By quickstart= By.xpath("html/body/div[2]/section[1]/div/div/div/div[1]/form/div/div/div/div[1]/div/div[2]/div/div/div[1]/spanf");
	private By menubar = By.xpath("html/body/div[1]/div[3]/div/div/header/nav/ul/li[3]/a/label");
	private By reportacomplaintinMenuBar = By.xpath("html/body/div[1]/div[3]/div/div/header/nav/div/ul/li[2]/a[contains(text(),'Report a Complaint')]");
	private By reportacomplaintinMenuBar1 =By.xpath("html/body/div[1]/div[3]/div/div/header/nav/div/ul/li[2]");
	private By complainagainstTextBox= By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[2]/div/input[1]");
	private By complainttypecompany = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[5]/div/div/input[1]");
	private By complainttypeperson = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[5]/div/div/input[2]");
	private By complainttypephone = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[5]/div/div/input[3]");
	private By changeComplainAgainstLink = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[3]/div/p/a");
	private By wishToFileAcomplaintAgainst = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[2]/div/h2");
	private By monetarydamagesText = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[4]/div[2]/input");
	private By companyLocationText = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[4]/div[4]/input");
	private By detailsText = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[4]/div[5]/textarea");
	private By submitbtn = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[3]/input");	
	private By calenderButton = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[4]/div[3]/input");	
	private By calendarmb = By.xpath("html/body/div[10]");
	private By navigattomonth = By.xpath("html/body/div[10]/div/a[1]");
	private By usernamemb = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[1]/div[2]/input");
	private By emailaddressmb = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[1]/div[3]/input");
	private By theirlocationmb = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[1]/div[4]/input");
	private By selectacarriermb = By.xpath("html/body/div[1]/div[3]/div[1]/form/div[2]/div[2]/div[6]/div/div[1]/div[2]/select");
	private By selectdatefromcal = By.xpath("html/body/div[10]/table/tbody/tr[1]/td[2]/a");
	private By calenderpreviousbutton = By.xpath("html/body/div[10]/div/a[1]/span");
	
	private By businessUserButton = By.xpath("html/body/div[2]/div[1]/div[1]/div/div/ul/li[2]/a[contains(text(),'I am a business user')]");
	private By firstNameBU = By.id("first_name");
	private By lastNameBU = By.id("last_name");
	private By companyNameBU = By.xpath("html/body/div[2]/div[2]/section/div/div/div/div[1]/div/form/div[2]/div[1]/input");
	private By companyurlBU = By.id("company_website");
	private By emailaddressBU =  By.id("email");
	private By phonenumberin = By.xpath("html/body/div[2]/div[2]/section/div/div/div/div[1]/div/form/div[3]/div[2]/div/div/div");
	private By phonenumberBU = By.id("phone");
	private By signupnowbuttonBU = By.xpath("html/body/div[2]/div[2]/section/div/div/div/div[1]/div/form/button");
	
	//public scambookPage(){}
	public scambookPage(WebDriver driver) {
		this.driver = driver;
	}
	
	ExcelLibrary excel = new ExcelLibrary();
	

	
	
	public static void scrollTo(WebDriver driver, WebElement element) {
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].scrollIntoView();", element);
    }
	
	public boolean findquickstart()
	{
		try
		{
		WebElement findquickstart = driver.findElement(quickstart);
		scrollTo(driver,findquickstart);
		return true;
		}
		catch(WebDriverException ignored)
		{
			return false;
		}
	}
	
//Clicking on "i am here to report a scam" button
	public boolean clickonImheretoreportscamButton() 
	{
		try
		{
			System.out.println("\tClicking on I'm heret to report a Scam Button");
			
			WebElement imheretoreportscam = driver.findElement(imheretoreportascam);
			if(imheretoreportscam.isEnabled()||imheretoreportscam.isDisplayed())
			{
				imheretoreportscam.click();
			}
			else
				System.out.println("INCORRECT ---> I'm here to report a scam button is not found");
			
			WebElement complaintForm = driver.findElement(complaintform);
			if(complaintForm.isDisplayed()||complaintForm.isEnabled())
				System.out.println("CORRECT ---> Navigated to Complaint Form Section");
			else
				System.out.println("INCORRECT -X-> Failed to navigate to Complaint Form Section");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
					
	}
	
	
	
//Operating on "complaint against" Text Field against Company
	public boolean fillingComplaintAgainstTextFild(String companyname)
	{
		try
		{
			System.out.println("\tClick and filling in 'complaint against' Textbox against Company");
			
			WebElement  complaintAgainst = driver.findElement(complaintagainst);
			if(complaintAgainst.isEnabled()||complaintAgainst.isDisplayed())
			{
				complaintAgainst.sendKeys(companyname);
				WebElement complainagainstTab = driver.findElement(complaintagainsttabindex);
				WebElement complainagainstClass = driver.findElement(complaintagainstclass);
				if((complainagainstTab.isDisplayed()||complainagainstTab.isEnabled())) complainagainstTab.click();
				else if((complainagainstClass.isDisplayed()||complainagainstClass.isEnabled())) complainagainstClass.click();
				
				System.out.println("CORRECT ---> Data entered in 'Complain tAgainst' TextBox against Company");
			}
			else System.out.println("INCORRECT -X-> 'Complaint against' Textbox is not found");
			return true;	
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
//Operating on "complaint against" Text Field against Company
	public boolean fillingComplaintAgainstTextFild_person(String personName) 
	{
		try
		{
			System.out.println("\tClick and filling in 'complaint against' Textbox against Company");
			
			WebElement  complaintAgainst = driver.findElement(complaintagainst);
			if(complaintAgainst.isEnabled()||complaintAgainst.isDisplayed())
			{
				complaintAgainst.sendKeys(personName);
				Thread.sleep(3000);
				
				System.out.println("CORRECT ---> Data entered in 'Complain tAgainst' TextBox as ->"+personName);
			}
			else System.out.println("INCORRECT -X-> 'Complaint against' Textbox is not found");
			return true;	
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
//Operating on "complaint against" Text Field against Person
	//JavascriptExecutor je =(JavascriptExector) driver;
	public boolean fillingComplaintAgainstTextFielddasPerson(String Person)
	{
		try
		{
			System.out.println("\tClick and filling in 'complaint against' Textbox against Person");
			
						
			WebElement  complaintAgainst = driver.findElement(complaintagainst);
			if(complaintAgainst.isEnabled()||complaintAgainst.isDisplayed())
			{
				complaintAgainst.sendKeys(Person);
				System.out.println("CORRECT ---> Data entered in 'Complain tAgainst' TextBox against Person");
			}
			else System.out.println("INCORRECT -X-> 'Complaint against' Textbox is not found ");
			return true;	
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}	
	
	
//Operating on "complaint against" Text Field against Person
	public boolean fillingComplaintAgainstTextFildasPhoneNumber(String PhoneNumber)
	{
		try
		{
			System.out.println("\tClick and filling in 'complaint against' Textbox against PhoneNumber");
			
			WebElement  complaintAgainst = driver.findElement(complaintagainst);
			if(complaintAgainst.isEnabled()||complaintAgainst.isDisplayed())
			{
				complaintAgainst.sendKeys(PhoneNumber);
				System.out.println("CORRECT ---> Data entered in 'Complain tAgainst' TextBox against PhoneNumber");
			}
			else System.out.println("INCORRECT -X-> 'Complaint against' Textbox is not found ");
			return true;	
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
	}	
	
	
//Operating on select Type SELECT drop down
	public boolean clickonSelecttype() 
	{
		try
		{
			System.out.println("\tClicking on Select Type dropdown...");
			WebElement selecttypedropdown = driver.findElement(selecttype);
			if(selecttypedropdown.isEnabled()||selecttypedropdown.isDisplayed())
			{
				selecttypedropdown.click();
				System.out.println("CORRECT ---> clicked on Select type dropdown");
			}
			else
				System.out.println("INCORRECT -X-> Select Type is not displayed");
			return true;
			}
			catch (NoSuchElementException ignored){   
				return false;
		}
	}
//Operating on select Type SELECT drop down
	public boolean clickonSelecttype1() 
	{
		try
		{
			System.out.println("\tClicking on Select Type dropdown...");
			WebElement selecttypedropdown = driver.findElement(selecttype1);
			if(selecttypedropdown.isEnabled()||selecttypedropdown.isDisplayed())
			{
				selecttypedropdown.click();
				System.out.println("CORRECT ---> clicked on Select type dropdown");
			}
			else
				System.out.println("INCORRECT -X-> Select Type is not displayed");
			return true;
			}
			catch (NoSuchElementException ignored){   
				return false;
		}
	}
	//Selecting Company from the Select type Dropdown
	public boolean selectCompanyfromDropdown()
	{
		try
		{
			System.out.println("\tSelecting Company from the Select Type Dropdown");
			
			WebElement selectCompany = driver.findElement(selectcompany);
			if(selectCompany.isEnabled()||selectCompany.isDisplayed())
			{
				selectCompany.click(); System.out.println("CORRECT ---> Company is selected from the dropdown");
			}
			else System.out.println("INCORRECT -X-> Company Option in not visible");
			return true;     
		}
		catch (NoSuchElementException ignored){   
			return false;
	}
	}

	//Selecting Person from the Select type Dropdown
	public boolean selectPersonfromDropdown() 
	{
		try
		{
			System.out.println("\tSelecting Person from the Select Type Dropdown");
			WebElement selectPerson = driver.findElement(selectperson);
			if(selectPerson.isEnabled()||selectPerson.isDisplayed())
			{
				selectPerson.click(); 
				System.out.println("CORRECT ---> Person is selected from the dropdown");
			}
			else System.out.println("INCORRECT -X-> Person Option in not visible");
			return true;
		}
		catch (NoSuchElementException ignored){   
			return false;
	}
	}

	//Selecting Phone from the Select type Dropdown
	public boolean selectPhonefromDropdown() //throws InterruptedException
	{
		try
		{
			System.out.println("\tSelecting Phone from the Select Type Dropdown");
			WebElement selectPhone = driver.findElement(selectphone);
			if(selectPhone.isEnabled()||selectPhone.isDisplayed())
			{
					selectPhone.click(); System.out.println("CORRECT ---> Phone is selected from the dropdown");
			}
			else System.out.println("INCORRECT -X-> Phone Option in not visible");
			return true;
		}
		catch (NoSuchElementException ignored){   
			return false;
		}
			catch(WebDriverException ignored){
				return false;
			} 
	}
	
	
	
	
//Operating on Company's Locatioin Text field
	public boolean clickingonCompanyLocationText(String companylocation)
	{
		try
		{
			System.out.println("\tClicking on Company's Location Text field...");
			
			WebElement companysLocatonText = driver.findElement(companylocaton);
			if(companysLocatonText.isEnabled()||companysLocatonText.isDisplayed())
			{
				companysLocatonText.sendKeys(companylocation);
				System.out.println("CORRECT ---> Company Location is filled");
			}
			else	System.out.println("INCORRECT -X-> Company Location Field is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
	}
	}
	
	
	
//Operating on Monetary Damages Text 
	public boolean fillingonMonetaryDamagesText(String monetaryamont)
	{
		try
		{
			System.out.println("\tClicking on Monetary Damages Text field...");
			
			WebElement MonetaryDamagesText = driver.findElement(monetarydamages);
			if(MonetaryDamagesText.isEnabled()||MonetaryDamagesText.isDisplayed())
			{
				MonetaryDamagesText.sendKeys(monetaryamont);
				System.out.println("CORRECT ---> Monetary Damages is filled");
			}
			else	System.out.println("INCORRECT -X-> Monetary Damages Field is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
	}
	}
	
	            
	
//Operating on the Calendar 
	public boolean fillingDatefromCalendar(String date)  
	{      
		try
		{           
			System.out.println("\tClicking on Calender and Selecting Date ...");
			
			WebElement dateSelect = driver.findElement(datebutton);
			if(dateSelect.isDisplayed()||dateSelect.isEnabled())
			{
				dateSelect.sendKeys(date);
				Actions act = new Actions(driver);
				act.sendKeys(Keys.ENTER).perform();
				System.out.println("CORRECT ---> Date button is present and clicked on it");				
			}
			else System.out.println("INCORRECT -X-> Date button is NOT found");
			
			return true;
		}
			 
		catch (NoSuchElementException ignored){
			return false;
	}
	}
	

	
	
//Operating on Details Text Body
	public boolean fillingonDetailsText(String detail)
	{
		try
		{
			System.out.println("\tClicking on Details Text field...");
			
			WebElement DetailsText = driver.findElement(details);
			if(DetailsText.isEnabled()||DetailsText.isDisplayed())
			{
				DetailsText.sendKeys(detail);
				System.out.println("CORRECT ---> Details is filled");
			}
			else	System.out.println("INCORRECT -X-> Details Field is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
	}
	}
	
	
//Operating on submit button
	public boolean clickingonSubmitButton()
	{
		try
		{   
			System.out.println("\tClicking on Submit button ...");
			WebElement submitbutton = driver.findElement(submit);
			if(submitbutton.isEnabled()||submitbutton.isDisplayed())
			{
				submitbutton.click();
				System.out.println("CORRECT ---> Submit button is clicked");
			}
			else System.out.println("INCORRECT -X-> Submit button Not Found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
//verifying the Login Page 
	public boolean verifyingtheLoginPage()
	{
		try
		{
			System.out.println("\tVerifying the Login page is opened or not ...");
			
			//WebElement LoginLink = driver.findElement(login);
			WebElement createAccountText = driver.findElement(createAccount);
			if(createAccountText.isEnabled()||createAccountText.isDisplayed())
				System.out.println("CORRECT ---> Loginpage is opened SUCCESSFULLY");
			else
				System.out.println("INCORRECT -X-> Loginpage is NOT opend");
			
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
		
	}
	
	
	
	
	
//verifying the Login Page 
	public boolean verifyingtheLoginPageMB()
	{
		try
		{
			System.out.println("\tVerifying the Login page is opened or not ...");
			
			//WebElement LoginLink = driver.findElement(login);
			WebElement createAccountText = driver.findElement(createAccountMB);
			if(createAccountText.isEnabled()||createAccountText.isDisplayed())
				System.out.println("CORRECT ---> Loginpage is opened SUCCESSFULLY");
			else
				System.out.println("INCORRECT -X-> Loginpage is NOT opend");
			
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
		
	}
	
	
	
	
//Operation on "Their Username" TextField
	public boolean fillingTheirUserNameTextField(String Username)
	{
		try
		{
			System.out.println("\tEntering the data in 'Their Username' Textfield");
			WebElement theirusernameText = driver.findElement(username);
			if(theirusernameText.isEnabled()||theirusernameText.isDisplayed())
			{
				theirusernameText.sendKeys(Username);
				System.out.println("CORRECT ---> Username Text field is filled");
			}
			else
				System.out.println("INCORRECT -X-> Username Text field is NOT found");
			
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	

//Operation on "Their EmailAddress" TextField
	public boolean fillingTheirEmailaddressTextField(String Username)
	{
		try
		{
			System.out.println("\tEntering the data in 'EmailAddress' Textfield");
			WebElement theirEmailaddressText = driver.findElement(emailaddress);
			if(theirEmailaddressText.isEnabled()||theirEmailaddressText.isDisplayed())
			{
				theirEmailaddressText.sendKeys(Username);
				System.out.println("CORRECT ---> EmailAddress Text field is filled");
			}
			else
				System.out.println("INCORRECT -X-> EmailAddress Text field is NOT found");
			
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//Operation on "Their Location" TextField
	public boolean fillingTheirLocationTextField(String Username)
	{
		try
		{
			System.out.println("\tEntering the data in 'Their Location' Textfield");
			WebElement theirLocationText = driver.findElement(theirlocation);
			if(theirLocationText.isEnabled()||theirLocationText.isDisplayed())
			{
				theirLocationText.sendKeys(Username);
				System.out.println("CORRECT ---> 'Their Location' Text field is filled");
			}
			else
				System.out.println("INCORRECT -X-> 'Their Location' Text field is NOT found");
			
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}

	
	
//Operation on "Select a Carrier" Dropdown                 >>'AT&T'
	public boolean selectacarrierDropdownChoosing_ATANDT()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown >> AT&T");
			
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("AT&T");
				System.out.println("CORRECT ---> 'AT&T' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown                 >>'T-MOBILE'
	public boolean selectacarrierDropdownChoosing_TMOBILE()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown >>'T-MOBILE'");
			
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("T-Mobile");
				System.out.println("CORRECT ---> 'T-Mobile' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown              		>>Verizon												   >>'T-MOBILE'
	public boolean selectacarrierDropdownChoosing_VERZION()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown  >>'T-MOBILE'");
			
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("Verizon");
				System.out.println("CORRECT ---> 'Verizon' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown                				>>Sprint											 >>'SPRINT'
		public boolean selectacarrierDropdownChoosing_SPRINT()
		{
			try
			{
				System.out.println("\tSelecting options from 'Select a carrier' Dropdown   >>'SPRINT'");
				
				WebElement selectacarrierDD= driver.findElement(selectcarrier);
				Select selecarr = new Select(selectacarrierDD);
				if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
				{
					selecarr.selectByVisibleText("Sprint");
					System.out.println("CORRECT ---> 'T-Mobile' is selected from the Select a carrier dropdown");
				}
				else	System.out.println("INCORRECT -X-> Dropdown is not found");
				return true;
			}
			catch (NoSuchElementException ignored){
				return false;
			}
		}
		//Operation on "Select a Carrier" Dropdown                 					>>MetroPcs										>>'METRO PCS'
	public boolean selectacarrierDropdownChoosing_METROpCS()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown  >>'METRO PCS'");
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("Metro PCS");
				System.out.println("CORRECT ---> 'Metro PCS' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown                					>>Boost Mobile									 >>'BOOST MOBILE'
	public boolean selectacarrierDropdownChoosing_BOOSTmOBILE()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown  >>'BOOST MOBILE'");
			
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("Boost Mobile");
				System.out.println("CORRECT ---> 'Boost Mobile' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown     								>>Virgin Mobile									 >>'VIRGIN MOBILES'    
	public boolean selectacarrierDropdownChoosing_VIRGINmobiles()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown   >>'VIRGIN MOBILES'");
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("Virgin Mobile");
				System.out.println("CORRECT ---> 'Virgin Mobile' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown 											>>Rogers							   	>>'ROGERS'
	public boolean selectacarrierDropdownChoosing_ROGERS()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown  >>'ROGERS'");
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("Rogers");
				System.out.println("CORRECT ---> 'Rogers' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//Operation on "Select a Carrier" Dropdown 								>>Other Carrier											>>'OTHER CARRIERS'
	public boolean selectacarrierDropdownChoosing_OTHERcARRIERS()
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown  >>'OTHER CARRIERS'");
			WebElement selectacarrierDD= driver.findElement(selectcarrier);
			Select selecarr = new Select(selectacarrierDD);
			if(selectacarrierDD.isEnabled()||selectacarrierDD.isDisplayed())
			{
				selecarr.selectByVisibleText("Other Carrier");
				System.out.println("CORRECT ---> 'Other Carrier' is selected from the Select a carrier dropdown");
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//Operating on BackButton
	public boolean clickingonBackButton()
	{
		try
		{
			System.out.println("\tClicking on Back Button ...");
			
			WebElement backbutton= driver.findElement(back);
			if(backbutton.isDisplayed()||backbutton.isEnabled())
			{
				backbutton.click();
				System.out.println("CORRECT ---> Back button is present and clicked");
			}
			else
				System.out.println("INCORRECT -X-> Back button NOT available");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	

	
	
	
	
	
	
//MENUBAR	
//Operation on MENU BAR   >> MENU BAR
	public boolean clickonMenuBar()
	{
		try
		{
			System.out.println("\tClicking on Menu Bar");
			WebElement menubarwe = driver.findElement(menubar);
			if(menubarwe.isDisplayed()||menubarwe.isEnabled())
			{
				menubarwe.click();
				System.out.println("CORRECT --> Menu Bar is cliked and the list is Displayed");
			}
			else
				System.out.println("INCORRECT -X-> Menu bar is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	

//Operation on REPORT A COMPLAINT in MENU BAR		>> REPORT A COMPLAINT
	public boolean clickonReportacomplaintMENUBAR() throws InterruptedException
	{
		try
		{
			System.out.println("\tClicking on Report a Complaint Link...");
			Thread.sleep(2000);
			WebElement reportcomplaintinMenuBar = driver.findElement(reportacomplaintinMenuBar);
			if(reportcomplaintinMenuBar.isDisplayed()||reportcomplaintinMenuBar.isEnabled())
			{
				reportcomplaintinMenuBar.click();
				System.out.println("CORRECT --> Report a Scam Button is Clicked in MENU BUTTON");
			}
			else	System.out.println("INCORRECT -X-> Report a Scam Button is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}	
	}
	public boolean clickonReportacomplaint1MENUBAR()// throws InterruptedException
	{
		try
		{
			System.out.println("\tClicking on Report a Complaint Link...");
			//Thread.sleep(2000);
			WebElement reportacomplaintin1MenuBarWE = driver.findElement(reportacomplaintinMenuBar1);
			if(reportacomplaintin1MenuBarWE.isDisplayed()||reportacomplaintin1MenuBarWE.isEnabled())
			{
				reportacomplaintin1MenuBarWE.click();
				System.out.println("CORRECT --> Report a Scam Button is Clicked in MENU BUTTON");
			}
			else	System.out.println("INCORRECT -X-> Report a Scam Button is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}	
	}
	
	
	private By listbox = By.xpath("//ul[@role='listbox']");
//Operatin on COMPLAINT AGAINST TEXTBOX		>>  COMPLAINT AGAINST TEXTBOX
	public boolean fillComplaintAgainstTextBoxMENUBAR(String complaint_against)
	{
		try
		{
			System.out.println("\tFilling in complaint against text box...");
			WebElement complainagainstTextBoxwe = driver.findElement(complainagainstTextBox);
			if(complainagainstTextBoxwe.isEnabled()||complainagainstTextBoxwe.isDisplayed())
			{
				complainagainstTextBoxwe.sendKeys(complaint_against);
				Thread.sleep(1000);
				System.out.println("CORRECT --> Complaint Against Text is filled");
				
				WebElement listboxwe = driver.findElement(listbox);
				if(listboxwe.isDisplayed())
				{
					Actions act = new Actions(driver);
					act.sendKeys(Keys.ARROW_DOWN).perform();
					act.sendKeys(Keys.ENTER).perform();
					Thread.sleep(1000);
				}
				else
					return true;
			}
			else
				System.out.println("INCORRECT -X-> Complaint Against Text field is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}	
	}
	
	
	
//Operating on COMPLAINT AGAINST BUTTON 						>> COMPANY
	public boolean clickonComplaintAgainstCompanyButtonMENUBAR()
	{
		try
		{
			System.out.println("\tClicking on complaint against Company Button ... ");
			WebElement complainttypecompanywe = driver.findElement(complainttypecompany);
			if(complainttypecompanywe.isEnabled()||complainttypecompanywe.isDisplayed())
			{
				complainttypecompanywe.click();
				System.out.println("CORRECT --> Complaint against Company Button is clicked");
			}
			else	System.out.println("INCORRECT -X-> Complaint against Company Button is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//															>>  PERSON 
	public boolean clickonComplaintAgainstPERSONButtonMENUBAR()
	{
		try
		{
			System.out.println("\tClicking on complaint against Company Button ... ");
			WebElement complainttypepersonwe = driver.findElement(complainttypeperson);
			if(complainttypepersonwe.isEnabled()||complainttypepersonwe.isDisplayed())
			{
				complainttypepersonwe.click();
				System.out.println("CORRECT --> Complaint against PERSON Button is clicked");
			}
			else	System.out.println("INCORRECT -X-> Complaint against PERSON Button is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	//															>>  PHONE NUMBER
	public boolean clickonComplaintAgainstPHONEnUMBERButtonMENUBAR()
	{
		try
		{
			System.out.println("\tClicking on complaint against PHONE NUMBER Button ... ");
			WebElement complainttypePHONEwe = driver.findElement(complainttypephone);
			if(complainttypePHONEwe.isEnabled()||complainttypePHONEwe.isDisplayed())
			{
				complainttypePHONEwe.click();
				System.out.println("CORRECT --> Complaint against PHONE NUMBER Button is clicked");
			}
			else	System.out.println("INCORRECT -X-> Complaint against PHONE NUMBER Button is NOT found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//Operatin on Change Complaint Against LINK  >> CHANGE 
	public boolean clickonCHANGEcomplanintAgainstLinkMENUBAR()
	{
		try
		{
			System.out.println("\tClicking on Change complaint Against ...");
			WebElement changeComplainAgainstLinkwe = driver.findElement(changeComplainAgainstLink);
			if(changeComplainAgainstLinkwe.isEnabled()||changeComplainAgainstLinkwe.isDisplayed())
			{
				changeComplainAgainstLinkwe.click();
				System.out.println("CORRECT --> Change the complaint against LINK is CLICKED");
			}
			else	System.out.println("INCORRECT -X-> change the complaint against LINK is NOT FOUND");
			
			WebElement wishToFileAcomplaintAgainstwe = driver.findElement(wishToFileAcomplaintAgainst);
			if(wishToFileAcomplaintAgainstwe.isDisplayed()||wishToFileAcomplaintAgainstwe.isEnabled())
				System.out.println("CORRECT --> Sucesssfully Navigated to File complaint Against Section ");
			else
				System.out.println("INCORRECT -X-> Failed to navigate to Complaint Against Text field");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
//Operating on MONETARY DAMAGE Text field >> MONETARY DAMAGE text
	public boolean fillinMonetaryDamageTextMENUNBAR(String Monetary_damage)
	{
		try
		{
			System.out.println("\tFilling in Monetary Damage Text Field ...");
			WebElement monetarydamagesTextwe = driver.findElement(monetarydamagesText);
			if(monetarydamagesTextwe.isDisplayed()||monetarydamagesTextwe.isEnabled())
			{
				monetarydamagesTextwe.sendKeys(Monetary_damage);
				System.out.println("CORRECT --> Sucessfuly filled in Monetary Damage TEXT FIELD");
			}
			else System.out.println("INCORRECT -X-> Failed to Fill in Monetary Damage Text Field");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
//Operatin on COMANY LOCATION Text field  >> COMPANY LOCATION
	public boolean fillinCompanyLocationTextMENUBAR(String Company_Location)
	{
		try
		{
			System.out.println("\tFilling in COMPANY LOCATION Text Field ...");
			WebElement companyLocationTextwe = driver.findElement(companyLocationText);
			if(companyLocationTextwe.isDisplayed()||companyLocationTextwe.isEnabled())
			{
				companyLocationTextwe.sendKeys(Company_Location);	
				System.out.println("CORRECT --> Sucessfully filled in Company Location Text Field");
			}
			else	System.out.println("INCORRECT -X-> Failed to Fill in Company Location Text Field");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
//Operating on DETAILS Text Field   >> DETAILS 
	public boolean fillinDetailsTextMENUBAR(String Details)
	{
		System.out.println("\tFilling in Details Text Field ...");
		try
		{
			WebElement detailsTextwe = driver.findElement(detailsText);
			if(detailsTextwe.isDisplayed()||detailsTextwe.isEnabled())
			{
				detailsTextwe.sendKeys(Details);
				System.out.println("CORRECT --> Sucessfully filled in Details Text Area");
			}
			else	System.out.println("INCORRECT -X-> Failed to fill in Details Text Area");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//Operating on SUBMIT Button  >> SUBMIT 
	public boolean clickonSubmitButtionMENUBAR()
	{
		System.out.println("\tClicking on Submit Button ...");
		try
		{
			WebElement submitbtnwe = driver.findElement(submitbtn);
			if(submitbtnwe.isDisplayed()||submitbtnwe.isEnabled())
			{
				submitbtnwe.click();
				System.out.println("CORRECT --> Sucessfully Clicked on Submit button");
			}
			else	System.out.println("INCORRECT -X-> Failed to Click on Submit button");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
//Operatin on CALERDAR BUTTON   >> CALENDAR Button
	public boolean clickonDateButtonMENUBAR()
	{
		try
		{
			System.out.println("\tClicking on Date Button ...");
			WebElement dateButton = driver.findElement(calenderButton);
			if(dateButton.isDisplayed()||dateButton.isEnabled())
			{
				dateButton.click();
				System.out.println("CORRECT --> Date Buton is clicked ");
			}
			else System.out.println("INCORRECT -X-> Date Button is NOT Found");
			Thread.sleep(5000);
			WebElement selectdate = driver.findElement(selectdatefromcal);
			selectdate.click();
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	//Opetatiin on CALENDAR   >> CALENDAR <SELECTIN DATE> 
	public boolean selectdatefromCALENDARMENUBAR(String happened_date) throws InterruptedException
	{
		try
		{
			System.out.println("\tSelectin date from the calendar ...");
			WebElement calendarvisible = driver.findElement(calendarmb);
			if(calendarvisible.isDisplayed()||calendarvisible.isEnabled())
			{
				WebElement navigatetoMonthButton = driver.findElement(navigattomonth);
				if(navigatetoMonthButton.isDisplayed()||navigatetoMonthButton.isEnabled())
				{
					String months = "";
					String dates = "";
					String years = "";
						for(int i=0;i<10;i++)
						{
							if(i<2)
							{
								months+=happened_date.charAt(i);
							}
							if((i<5)&&(i!=2))
							{
								dates+=happened_date.charAt(i);
							}
							if(i>5)
							{
								years+=happened_date.charAt(i);
							}
						}						
					int month =Integer.parseInt(months);
	//				int date = Integer.parseInt(dates);
					int year =Integer.parseInt(years);
			//selecting month	
					int yr=2016-year; int clks=0;
					if(yr!=0)
						clks=yr*5;
					int add=12-month;
					int clicks = clks+add;
					System.out.println(clicks);
					for(int i=0;i < clicks;i++)
					{
						navigatetoMonthButton.click();
						Thread.sleep(2000);
					}
					System.out.println("CORRECT --> Previous Month "+clicks+" times selected");
			//selecting date
					//DatePicker is a table.So  we have to navigate to each cell   
					   
					WebElement datepicker = driver.findElement(By.id("ui-datepicker-div"));  
			//		 List<WebElement> rows=datepicker.findElements(By.tagName("tr"));  
					List<WebElement> columns=datepicker.findElements(By.tagName("td"));  
					    
					for (WebElement cell: columns)
					{  
						if (cell.getText().equals(dates))
						{  
							cell.findElement(By.linkText(dates)).click();  
							break;  
						}  
					}   
				}
				else System.out.println("INCORRECT -X-> Previous Month is not displayed");	
			}
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	public boolean clickonDateButtonMENUBAR_NEW()
	{
		try
		{
			System.out.println("\tClicking on Date Button ...");
			WebElement dateButton = driver.findElement(calenderButton);
				dateButton.click();
				System.out.println("CORRECT --> Date Buton is clicked ");
			WebElement previousmonth = driver.findElement(calenderpreviousbutton);
			previousmonth.click();
			WebElement selectdate = driver.findElement(selectdatefromcal);
			selectdate.click();
			//WebElement datepicker = driver.findElement(By.id("ui-datepicker-div"));  
			// List<WebElement> rows=datepicker.findElements(By.tagName("tr"));  
			//List<WebElement> columns=datepicker.findElements(By.tagName("td"));  
			
			
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
//Operating on DATE BUTTON 							>> DATE BUTTON
	public boolean fillinDateButon_MENUBAR(int date)
	{
		try
		{
			System.out.println("\tFillin on the date button ...");
			
			driver.switchTo().frame(0); 
			 
			//Click on textbox so that datepicker will come  
			driver.findElement(By.id("dp1463481285107")).click();  
			 
			//Click on next so that we will be in next month  
			 driver.findElement(By.xpath("html/body/div[10]/div/a[1]/span")).click();
			  
			//DatePicker is a table.So  we have to navigate to each cell   
			   
			WebElement datepicker = driver.findElement(By.className("ui-state-defaultd"));  
		//	 List<WebElement> rows=datepicker.findElements(By.tagName("tr"));  
			List<WebElement> columns=datepicker.findElements(By.tagName("td"));  
			    
			for (WebElement cell: columns){  
			//Select 20th Date   
			if (cell.getText().equals(date)){  
			cell.findElement(By.linkText("date")).click();  
			break;  
			 }  
		}
			return true;
		}
			catch (NoSuchElementException ignored){
				return false;
			}
	}
	
	
	
//Operatin on USERNAME Text field 					>> USER NAME Text field
	public boolean fillinTheirUsernameTextMENUBAR(String Username)
	{
		System.out.println("\tFilling in USER NAME Text field...");
		try
		{
			WebElement usernameTextwe = driver.findElement(usernamemb);
			if(usernameTextwe.isDisplayed()||usernameTextwe.isEnabled())
			{
				usernameTextwe.sendKeys(Username);
				System.out.println("CORRECT --> Sucessfully filled in USER NAME Text field");
			}
			else	System.out.println("INCORRECT -X-> Failed to fill in USER NAME Text field");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//Operatin on EMAIL ADDRESS Text field 					>> EMAIL ADDRESS Text field
	public boolean fillinEMAILaDDRESSTextMENUBAR(String emailid)
	{
		System.out.println("\tFilling in USER NAME Text field...");
		try
		{
			WebElement emailaddressTextwe = driver.findElement(emailaddressmb);
			if(emailaddressTextwe.isDisplayed()||emailaddressTextwe.isEnabled())
			{
				emailaddressTextwe.sendKeys(emailid);
				System.out.println("CORRECT --> Sucessfully filled in EMAIL ADDRESS Text field");
			}
			else	System.out.println("INCORRECT -X-> Failed to fill in EMAIL ADDRESS Text field");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//Operatin on THEIR LOCATION Text field 					>> THEIR LOCATION Text field
	public boolean fillinTheirLocationTextMENUBAR(String location)
	{
		System.out.println("\tFilling in USER NAME Text field...");
		try
		{
			WebElement theirLocationTextwe = driver.findElement(theirlocationmb);
			if(theirLocationTextwe.isDisplayed()||theirLocationTextwe.isEnabled())
			{
				theirLocationTextwe.sendKeys(location);
				System.out.println("CORRECT --> Sucessfully filled in THEIR LOCATION Text field");
			}
			else	System.out.println("INCORRECT -X-> Failed to fill in THEIR LOCATION Text field");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
	
	
	
//Operation on the Phone Company DropDown 									>> Phone Company DropDown
	public boolean selectaoptionfrom_PhoneCompany_Dropdown(String selection)
	{
		try
		{
			System.out.println("\tSelecting options from 'Select a carrier' Dropdown >> AT&T");
			
			WebElement selectacarrierDDWE= driver.findElement(selectacarriermb);
			Select selecarr = new Select(selectacarrierDDWE);
			if(selectacarrierDDWE.isEnabled()||selectacarrierDDWE.isDisplayed())
			{
				selecarr.selectByVisibleText(selection);
				System.out.println("CORRECT --> Selected Option from the DropDown is "+selection);
			}
			else	System.out.println("INCORRECT -X-> Dropdown is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}
	
	
	
	
//operatin on BusinesUser button 
	public boolean click_BusinessUser_Button()
	{
		try
		{
		//	System.out.println("\tclicking on Business User Button  ...");
			WebElement businessUserButtonwe = driver.findElement(businessUserButton);
			if(businessUserButtonwe.isDisplayed()||businessUserButtonwe.isEnabled())
			{
				businessUserButtonwe.click();	
				System.out.println("CORRECT ---> clicked on 'I'm a Business User' Button ");
			}
			else	System.out.println("INCORRECT -X-> 'I'm a Business User' Button is not found");
			comm.waitThreeSec();
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	
//operatin on First Name Text field 
	public boolean fill_firstname_Textfield(String Fname)
	{
		try
		{
			fname = Fname;
		//	System.out.println("\tEntering data in First name Text field ...");
			WebElement firstNameBUwe = driver.findElement(firstNameBU);
			if(firstNameBUwe.isDisplayed()||firstNameBUwe.isEnabled())
			{
				firstNameBUwe.sendKeys(Fname);	
				System.out.println("CORRECT ---> First name text field is filled as <"+Fname+">");
			}
			else	System.out.println("INCORRECT -X-> First name text field is NOT found <ERROR>");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}
	
	
	
//operatin on Last name Text field 
	public boolean fill_lastname_Textfield(String Lname)
	{
		try
		{
			 lname = Lname;
		//	System.out.println("\tEntering data in Last name Text field ...");
			WebElement lastNameBUwe = driver.findElement(lastNameBU);
			if(lastNameBUwe.isDisplayed()||lastNameBUwe.isEnabled())
			{
				lastNameBUwe.sendKeys(Lname);	
				System.out.println("CORRECT ---> Last name text field is filled as <"+Lname+">");
			}
			else	System.out.println("INCORRECT -X-> Last name text field is NOT found <ERROR>");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		}
	}	
	
	
//operatin on Company Name TextField 
	public boolean fill_CompanyName_Textfield(String comp)
	{
		try
		{
		//	System.out.println("\tEntering data in company name Text field ...");
			WebElement companyNameBUwe = driver.findElement(companyNameBU);
			if(companyNameBUwe.isDisplayed()||companyNameBUwe.isEnabled())
			{
				companyNameBUwe.sendKeys(comp);	
				System.out.println("CORRECT ---> company name text field is filled as <"+comp+">");
			}
			else	System.out.println("INCORRECT -X-> company name text field is NOT found <ERROR>");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}	
	
	
	
//operatin on Company url TextField 
	public boolean fill_CompanyUrl_Textfield(String compurl)
	{
		try
		{
		//	System.out.println("\tEntering data in company url Text field ...");
			WebElement companyurlBUwe = driver.findElement(companyurlBU);
			if(companyurlBUwe.isDisplayed()||companyurlBUwe.isEnabled())
			{
				companyurlBUwe.sendKeys(compurl);	
				System.out.println("CORRECT ---> company url text field is filled as <"+compurl+">");
			}
			else	System.out.println("INCORRECT -X-> company url text field is NOT found <ERROR>");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}		
	
	
//operatin on email address TextField 
	public boolean fill_emailaddress_Textfield(String Email)
	{
		try
		{
			email = Email;
		//	System.out.println("\tEntering data in email address Text field ...");
			WebElement emailaddressBUwe = driver.findElement(emailaddressBU);
			if(emailaddressBUwe.isDisplayed()||emailaddressBUwe.isEnabled())
			{
				emailaddressBUwe.sendKeys(Email);	
				System.out.println("CORRECT ---> email address text field is filled as <"+Email+">");
			}
			else	System.out.println("INCORRECT -X-> email address text field is NOT found <ERROR>");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}	
	
	
	
	

//operatin on phone number TextField 
	public boolean fill_phonenumber_Textfield(String Phone)
	{
		try
		{
			phone = Phone;
		//	System.out.println("\tEntering data in phone number Text field ...");
			WebElement phonenumberBUwe = driver.findElement(phonenumberBU);
			if(phonenumberBUwe.isDisplayed()||phonenumberBUwe.isEnabled())
			{
				phonenumberBUwe.sendKeys(Phone);	
				System.out.println("CORRECT ---> phone number text field is filled as <"+Phone+">");
			}
			else	System.out.println("INCORRECT -X-> phone number text field is NOT found <ERROR>");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}		




//operatin on sign up button 
	public boolean click_signupnowbu_button()
	{
		try
		{
		//	System.out.println("\tclicking the sign up now button ...");
			WebElement signupnowbuttonBUwewe = driver.findElement(signupnowbuttonBU);
			if(signupnowbuttonBUwewe.isDisplayed()||signupnowbuttonBUwewe.isEnabled())
			{
				signupnowbuttonBUwewe.click();	
				System.out.println("CORRECT ---> sign up button is clicked");
			}
			else	System.out.println("INCORRECT -X-> sign up button is not found");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}		



//operatin on phone number <country>
	public boolean click_phonePIN_button()
	{
		try
		{
		//	System.out.println("\tclickin and selecting the indian pincode for entering phone number ...");
			WebElement phonenumberinwe = driver.findElement(phonenumberin);
			if(phonenumberinwe.isDisplayed()||phonenumberinwe.isEnabled())
			{
				Actions act = new Actions(driver);

				phonenumberinwe.click();
				act.sendKeys("I").perform();
				act.sendKeys(Keys.ARROW_DOWN).perform();
				act.sendKeys(Keys.ENTER).perform();
				
				System.out.println("CORRECT ---> Selected the India pincode to enter the phone number");
			}
			else	System.out.println("INCORRECT -X-> No such element found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} 
		
	}		

	


//operatin on Business resolve page 
	public boolean verify_businessResolvePage_isdisplayed()
	{
		try
		{
			System.out.println("\tverifying the business resolve page is displayed or not ...");
			Thread.sleep(2000);
			String expectedtitle = "Business Resolve";
			String actualtitle = driver.getTitle();
			if(actualtitle.contains(expectedtitle))
			{
				System.out.println("CORRECT ---> Business Resolve | Scambook page is Displayed");
			}
			else	System.out.println("INCORRECT -X-> Business Resolve | Scambook page is NOT FOUND/Displayed");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} 
		
	}		

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
