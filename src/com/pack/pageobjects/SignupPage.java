package com.pack.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignupPage 
{


	private WebDriver driver;
	
	
	private By login = By.xpath("html/body/div[1]/div[3]/div/div/header/nav/ul/li[2]/a");
	private By signupnow = By.xpath("html/body/div[1]/div[1]/div/div/div[2]/div/form/div[4]/a[2]");
	private By firstname= By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/ul[1]/li[1]/div/input");
	private By lastname= By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/ul[1]/li[2]/div/input");
	private By emailaddress= By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/div[2]/input");
	private By password= By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/ul[2]/li[1]/div/input");
	private By confirmpassword= By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/ul[2]/li[2]/div/input");
	private By iagree = By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/div[4]/input");
	private By createaccount = By.xpath("html/body/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/form/input");
	
	
	
	
	public SignupPage(WebDriver driver)
	{
		this.driver = driver;
	}
	
	
	
	
//operation on login link
	public boolean click_login_link()
	{
		try
		{
			//System.out.println("Clicking on Login Link ....");
			WebElement loginlink = driver.findElement(login);
			if(loginlink.isEnabled()||loginlink.isDisplayed())
			{
				loginlink.click();
				System.out.println("CORRECT --> Login Link is clicked ");
			}
			else
				System.out.println("INCORRECT --> Login Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
	
	
	
//operation on signup now link in login popup page
	public boolean click_signupNow_link()
	{
		try
		{
			//System.out.println("Clicking on signupnow Link ....");
			WebElement signupnowwe = driver.findElement(signupnow);
			if(signupnowwe.isEnabled()||signupnowwe.isDisplayed())
			{
				signupnowwe.click();
				System.out.println("CORRECT --> signupnow Link is clicked ");
			}
			else
				System.out.println("INCORRECT --> signupnow Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
	
	
	
//operation on firstName text box in signup page
	public boolean fill_firstName_testBox(String firstName)
	{
		try
		{
			//System.out.println("Filling Firstname text box ....");
			WebElement firstnamewe = driver.findElement(firstname);
			if(firstnamewe.isEnabled()||firstnamewe.isDisplayed())
			{
				firstnamewe.sendKeys(firstName);
				System.out.println("CORRECT --> firstname Text box is filled ");
			}
			else
				System.out.println("INCORRECT --> signupnow Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}
	
	
//operation on lasttName text box in signup page	
	public boolean fill_lastName_testBox(String lastName)
	{
		try
		{
			//System.out.println("Filling secondName text box ....");
			WebElement lastnamewe = driver.findElement(lastname);
			if(lastnamewe.isEnabled()||lastnamewe.isDisplayed())
			{
				lastnamewe.sendKeys(lastName);
				System.out.println("CORRECT --> lastName Text box is filled ");
			}
			else
				System.out.println("INCORRECT --> lastName Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}	
	
	
	
//operation on emailaddress text box in signup page	
	public boolean fill_emailaddress_testBox(String mailid)
	{
		try
		{
			//System.out.println("Filling emailaddress text box ....");
			WebElement emailaddresswe = driver.findElement(emailaddress);
			if(emailaddresswe.isEnabled()||emailaddresswe.isDisplayed())
			{
				emailaddresswe.sendKeys(mailid);
				System.out.println("CORRECT --> emailaddress Text box is filled ");
			}
			else
				System.out.println("INCORRECT --> emailaddress Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
//operation on password text box in signup page	
	public boolean fill_password_testBox(String pswd)
	{
		try
		{
			//System.out.println("Filling password text box ....");
			WebElement passwordwe = driver.findElement(password);
			if(passwordwe.isEnabled()||passwordwe.isDisplayed())
			{
				passwordwe.sendKeys(pswd);
				System.out.println("CORRECT --> password Text box is filled ");
			}
			else
				System.out.println("INCORRECT --> password Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	


//operation on confirmpassword text box in signup page	
	public boolean fill_confirmpassword_testBox(String cpswd)
	{
		try
		{
			//System.out.println("Filling confirmpassword text box ....");
			WebElement confirmpasswordwe = driver.findElement(confirmpassword);
			if(confirmpasswordwe.isEnabled()||confirmpasswordwe.isDisplayed())
			{
				confirmpasswordwe.sendKeys(cpswd);
				System.out.println("CORRECT --> confirmpassword Text box is filled ");
			}
			else
				System.out.println("INCORRECT --> confirmpassword Link is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}	
	
	
	
	

//operation on confirmpassword text box in signup page	
	public boolean click_Iagree_checkbox()
	{
		try
		{
			//System.out.println("Clickin on I agree CheckBox ....");
			WebElement iagreewe = driver.findElement(iagree);
			if(iagreewe.isEnabled()||iagreewe.isDisplayed())
			{
				iagreewe.click();
				boolean chk = iagreewe.isSelected();
				if(chk)
					System.out.println("CORRECT --> Iagree Check box is Ticked ");
				else
					System.out.println("INCORRECT --> Iagree check box is not Ticked");
			}
			else
				System.out.println("INCORRECT --> Iagree Check box is NOT found ");
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			}
	}		
	
	
	
	
//operation on create account button login popup page
	public boolean click_createAccount_Button()
	{
		try
		{
			//System.out.println("Clicking on createaccountwe Link ....");
			WebElement createaccountwe = driver.findElement(createaccount);
			if(createaccountwe.isEnabled()||createaccountwe.isDisplayed())
			{
				createaccountwe.click();
				System.out.println("CORRECT --> createaccount Button is clicked ");
			}
			else
				System.out.println("INCORRECT --> createaccount Button is NOT found ");
			Thread.sleep(2000);
			return true;
		}
		catch (NoSuchElementException ignored){
			return false;
			} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}	





	
}
